package classes;


public class OwnClass_FollwoUP {

	private String Date;
	private String infarktYN;
	private String diedyetYN;
	private String InfarktDate;
	private String id;
	
	
	
	public OwnClass_FollwoUP(String id, String Date, String infarkt, String diedyet, String infarktDate ){
		
		setID(id);
		setDate(Date);
		setInfarktYN(infarkt);
		setDiedyetYN(diedyet);
		setInfarktDate(infarktDate);
		
		
	}

	/**
	 * @return the date
	 */
	
	
	
	
	
	
	
	
	
	
	public void setID(String id)
	{
		this.id = id;
	}
	
	
	public String getId()
	{
		return id;
	}
	
	
	public String getDate() {
		return Date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		Date = date;
	}

	/**
	 * @return the infarktYN
	 */
	public String getInfarktYN() {
		return infarktYN;
	}

	/**
	 * @param infarktYN the infarktYN to set
	 */
	public void setInfarktYN(String infarktYN) {
		this.infarktYN = infarktYN;
	}

	
	/**
	 * @return the infarktDate
	 */
	public String getInfarktDate() {
		return InfarktDate;
	}

	/**
	 * @param infarktDate the infarktDate to set
	 */
	public void setInfarktDate(String infarktDate) {
		InfarktDate = infarktDate;
	}

	/**
	 * @return the diedyetYN
	 */
	public String isDiedyetYN() {
		return diedyetYN;
	}

	/**
	 * @param diedyetYN the diedyetYN to set
	 */
	public void setDiedyetYN(String diedyetYN) {
		this.diedyetYN = diedyetYN;
	}

}
