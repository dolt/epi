package model;

import java.text.DateFormat;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

public class Model {
	
	
	final static String EMPTY = "";
	
	
	
	public String booleantoSTring(boolean bool){
		if(bool)
		return "ja";
		return "nein";
		
	}
	
	
	//gibt leeren String zur�ck
	
	public String clearTextField()
	{
		return EMPTY;
		
	}
	
	
	
	//pr�ft ob Feld leer ist
	
	public boolean isNull(String entry)
	{
		
		if(entry.equals(""))
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	
	//pr�ft ob ein zweiter Wert kleiner ist als der erste (zB bei BSG NICHT m�glich...)
	
	
	public boolean secondSmallerThanFirst(String entry1, String entry2)
	{
		
		int value1 = Integer.parseInt(entry1);
		int value2 = Integer.parseInt(entry2);
		
		if(value1 > value2)
		{
			return true;
		}
		
		else
		{
			return false;
		}
		
		
	}
	
	
	
	//pr�ft eine benutzerdefinierbare Range
	
	public boolean isOutOfRange(String entry, int min, int max)
	{
		
		int value = Integer.parseInt(entry);
		
		
		if(value < min || value > max)
		{
			
			return true;
			
		}
		else
		{
			return false;
		}
		
	}
	
	
	
	
	//pr�ft ob es sich bei dem Inhalt um eine Zahl handelt!
	
	public boolean isNumber(String entry)
	{
				
		if(entry.matches("\\d*") || entry.matches(".")) 
		{
	
			return true;
			
		}else{
			
			return false;
		}
		
	}
	
	
	
	
	
	
	
	
	
	//pr�ft das heutige Datum
	
	/*
	public boolean isDateInFuture(String enteredDate)
	{
		
		GregorianCalendar now	=	new GregorianCalendar();
		DateFormat df			=	DateFormat.getDateInstance(DateFormat.MEDIUM);
		String date 			= 	df.format(now.getTime());
	
	
		int day1 = Integer.parseInt(date.substring(0,2));
		int day2 = Integer.parseInt(enteredDate.substring(0,2));
		
		int month1 = Integer.parseInt(date.substring(3,5));
		int month2 = Integer.parseInt(enteredDate.substring(3,5));
		
		int year1 = Integer.parseInt(date.substring(6,9));
		int year2 = Integer.parseInt(enteredDate.substring(6,10));
		
		
		
		
		if((year1 < year2) || (day1 < day2 && month1 == month2 && year1 == year2) || (month1<month2 && year1==year2)  )
		{
			
			JOptionPane.showMessageDialog(null, "Gew�hltes Datum liegt in der Zukunft!");
			return true;
		}
		
		else
		{
		
			return false;
			
		}
		
	}
	
	*/
	
	

}
