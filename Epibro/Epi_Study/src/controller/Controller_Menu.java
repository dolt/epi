package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

import classes.Class_Expo_Aufnahme;
import classes.Class_Stammdaten;
import database.Query;
import model.Model;
import view.View_BodyCheck;
import view.View_Expo_Aufnahme;
import view.View_FollowUP;
import view.View_Labor_Aufnahme;
import view.View_Labor_Untersuchung;
import view.View_Menu;
import view.View_Stammdaten;

public class Controller_Menu {

	
	
	private View_Stammdaten 		viewStamm;
	private View_Menu				viewMenu;
	private Model					model;
	private View_Labor_Aufnahme 	viewLabA;
	private View_Labor_Untersuchung viewLabU;
	private View_Expo_Aufnahme		viewExpo;
	private View_FollowUP			viewFollow;
	private View_BodyCheck			viewBody;
	
	
	public Controller_Menu(View_Stammdaten viewStamm, View_Menu viewMenu, Model model, View_Labor_Aufnahme viewLabA, View_Labor_Untersuchung viewLabU, View_Expo_Aufnahme viewExpo, View_FollowUP viewFollow, View_BodyCheck viewBody)
	{
		
		this.viewStamm	= viewStamm;
		this.viewMenu	= viewMenu;
		this.model		= model;
		this.viewLabA	= viewLabA;
		this.viewLabU	= viewLabU;
		this.viewExpo	= viewExpo;
		this.viewFollow	= viewFollow;
		this.viewBody	= viewBody;
		
		viewMenu.addStammdatenAnlegenButtonListener(new StammdatenAnlegenButtonListener());
		viewMenu.addVerlassenButtonActionListener(new VerlassenButtonListener());
		viewMenu.addBearbeitenButtonListener(new BearbeitenButtonListener());
		
		
	}
	
	
	public class VerlassenButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			JOptionPane.showMessageDialog(null, "Sie haben sich abgemeldet!");
			
			viewMenu.dispose();
			viewExpo.dispose();
			viewLabA.dispose();
			viewLabU.dispose();
			viewStamm.dispose();
			viewFollow.dispose();
			viewBody.dispose();
		}
		
		
		
	}
	
	public class BearbeitenButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			if(viewMenu.getSelectedCase().equals("Fall auswählen"))
			{
				JOptionPane.showMessageDialog(null, "Bitte wählen Sie zuerst einen Fall aus");
			}
			
			else
			{
			
			viewMenu.setUmWasgehts("laden"); //werte laden -> 'laden' steht in hidden TF
			
			
			
			//ausgewählten Fall erhalten
			String fall_id = viewMenu.getSelectedCase();
			
			
			
			//Klasseninhalte leeren
			
			Class_Stammdaten stammdat = new Class_Stammdaten(fall_id, null, null, null, null, null);
			
			
			//Stammdaten des ausgewählten Falles in die Klasse laden (Klasse wird aus DaBa Methode übergeben)
			Query.loadStammdaten(fall_id, stammdat);
			 
			
			//spezifische FallID eintragen
			viewStamm.setTf_ID(fall_id);
			viewExpo.setTf_ID(fall_id);
			viewLabA.setTf_ID(fall_id);
			viewLabU.setTf_ID(fall_id);
			viewFollow.setTf_ID(fall_id);
			viewBody.setTf_ID(fall_id);
			
			//stammdaten anhand der Klasseninhalte füllen
			viewStamm.setZentrum(stammdat.getZentrum_stammdat());
			viewStamm.setGewicht(stammdat.getGewicht_kg_stammdat());
			viewStamm.setGroesse(stammdat.getGroesse_cm_stammdat());
			viewStamm.setDatumAufnahme(stammdat.getDatum_stammdat());
			viewStamm.setGeburtsdatum(stammdat.getGeburt_stammdat());
			
				 
			//View anzeigen
			viewStamm.setVisible(true);
			viewStamm.setLocationRelativeTo(null);
			
			
			
			
			//######################ALLE VIEWS MIT WERTEN BEFÜLLEN###########################
			
		
			Class_Expo_Aufnahme expo = new Class_Expo_Aufnahme(fall_id, null, null, null, null, null, null, null, null, null);
			
			Query.loadExpoAufnahme(fall_id, expo);
			
			viewExpo.setDatum(expo.getDatum_expo_aufnahme());
			
			if(expo.getRauchen().equals("nie"))
			{
				
				viewExpo.setRadioRauchenNie();
				
			}else{
				
				viewExpo.setRadioRauchenEhemals();
				
			}
			
			
			viewExpo.setAlterStartRauchen(expo.getRauchenBeginn());
			viewExpo.setJahreNichtraucher(expo.getRauchenEnde());
			viewExpo.setZigarettenAnzahl(expo.getRauchenAnzahl());
			
			
			if(expo.getAlkoholHäufigkeit().equals("nie"))
			{
				
				viewExpo.setRadioAlkoholNie();
				
			}
			else if(expo.getAlkoholHäufigkeit().equals("gelegentlich"))
			{
				
				viewExpo.setRadioAlkoholGelegentlich();
				
			}else{
				
				viewExpo.setRadioAlkoholTaeglich();
				
			}
			
			
			
			
			if(expo.getGesundheitlicheBedenken().equals("keine"))
			{
				
				viewExpo.setRadioBedenkenKeine();
				
			}else if(expo.getGesundheitlicheBedenken().equals("ja")){
				
				viewExpo.setRadioBedenkenKeine();
				
			}else{
				
				viewExpo.setRadioBedenkenVoraussetzung();
				
			}
			
			viewExpo.setAlkoholProTag(expo.getAlkoholGramm());
			
			
			
			
			if(expo.getRauchenArt().equals("Zigaretten, Pfeife")){
				
				viewExpo.setCBRauchenZigarette();
				viewExpo.setCBRauchPfeife();
				
			}else if(expo.getRauchenArt().equals("Zigaretten")){
				
				viewExpo.setCBRauchenZigarette();
				
			}else if(expo.getRauchenArt().equals("Pfeife")){
				
				viewExpo.setCBRauchPfeife();
				
			}
			
			}
		}
		
	}
	
	
	
	public class StammdatenAnlegenButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			
			
			String id = String.valueOf(Integer.parseInt(Query.getNewID()) + 1);
			viewMenu.setFallID(id);
			
			
			
			
			
			
			viewMenu.setUmWasgehts("neu"); //neu anlegen, deshalb alles weg -> 'neu' in hidden TF
			
			viewStamm.clearAll();
			viewExpo.clearAll();
			viewLabA.clearAll();
			viewLabU.clearAll();
			//viewFollow.clearAll();
			
			
			//Neue Fall ID in alle ID Textfelder schreiben
			viewStamm.setTf_ID(id);
			viewExpo.setTf_ID(id);
			viewLabA.setTf_ID(id);
			viewLabU.setTf_ID(id);
			viewFollow.setTf_ID(id);
			viewBody.setTf_ID(id);
			
			
			
			viewStamm.setVisible(true);
			viewStamm.setLocationRelativeTo(null);
			
		}
		
		
		
		
		
		
	}
	
	
}
