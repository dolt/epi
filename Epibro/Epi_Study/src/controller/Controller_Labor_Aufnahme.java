package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import classes.Class_Labor_Aufnahme;
import model.Model;
import view.View_Expo_Aufnahme;
import view.View_Labor_Aufnahme;
import view.View_Labor_Untersuchung;

public class Controller_Labor_Aufnahme {
	
	
	private Model model;
	private View_Expo_Aufnahme 		viewExpo;
	private View_Labor_Aufnahme		viewLabor;
	private View_Labor_Untersuchung	viewLaborU;
	
	public Controller_Labor_Aufnahme(Model model, View_Expo_Aufnahme viewExpo, View_Labor_Aufnahme viewLabor, View_Labor_Untersuchung viewLaborU)
	{
		
		this.model		= model;
		this.viewExpo	= viewExpo;
		this.viewLabor	= viewLabor;
		this.viewLaborU	= viewLaborU;
		
		
		
		viewLabor.addLaborWeiterButtonListener(new LaborWeiterButtonListener());
		viewLabor.addLaborZur�ckButtonListener(new LaborZur�ckButtonListener());
		
		
	}
	
	
	public class LaborWeiterButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) 
		{
		
			
			if(model.isNull(viewLabor.getDatumLaborAufnahme()) || model.isNull(viewLabor.getCholesterin()) || model.isNull(viewLabor.getCholesterin()) || model.isNull(viewLabor.getTriglyceride()) || model.isNull(viewLabor.getBSG1()) || model.isNull(viewLabor.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Textfelder aus!");
			}
			
			else if(model.isNumber(viewLabor.getCholesterin()) == false || model.isNumber(viewLabor.getCholesterin()) == false || model.isNumber(viewLabor.getTriglyceride()) == false || model.isNumber(viewLabor.getBSG1()) == false || model.isNumber(viewLabor.getBSG2()) == false )
			{
				JOptionPane.showMessageDialog(null, "Es befinden sich unerlaubte Zeichen in den Feldern!");
			}
			
			else if(model.isOutOfRange(viewLabor.getBSG1(), 0, 150) || model.isOutOfRange(viewLabor.getBSG2(), 0, 150))
			{
				
				JOptionPane.showMessageDialog(null, "BSG ausserhalb des m�glichen Bereiches!");
			}
			
			else if(model.secondSmallerThanFirst(viewLabor.getBSG1(), viewLabor.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "1. BSG ist h�her als 2. BSG!");
				
			}
			
			else
			{
				
				if(model.isOutOfRange(viewLabor.getCreatinin(), 40, 110))
				{
					JOptionPane.showMessageDialog(null, "!ACHTUNG!" + "\n" + "Creatinin liegt ausserhalb des physiologischen Messbereiches!");
				}
				
				
				
				Class_Labor_Aufnahme labA	= new Class_Labor_Aufnahme(viewLabor.getID(), 
																	viewLabor.getDatumLaborAufnahme(), 
																	viewLabor.getCholesterin(), 
																	viewLabor.getCreatinin(), 
																	viewLabor.getTriglyceride(),
																	viewLabor.getBSG1(), 
																	viewLabor.getBSG2());
				
				
				
				viewLabor.setVisible(false);
				
				viewLaborU.setVisible(true);
				viewLaborU.setLocationRelativeTo(null);
			}
			
			
		}
		
	}
	
	public class LaborZur�ckButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			
			
			if(model.isNull(viewLabor.getDatumLaborAufnahme()) || model.isNull(viewLabor.getCholesterin()) || model.isNull(viewLabor.getCholesterin()) || model.isNull(viewLabor.getTriglyceride()) || model.isNull(viewLabor.getBSG1()) || model.isNull(viewLabor.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Textfelder aus!");
			}
			
			else if(model.isNumber(viewLabor.getCholesterin()) == false || model.isNumber(viewLabor.getCholesterin()) == false || model.isNumber(viewLabor.getTriglyceride()) == false || model.isNumber(viewLabor.getBSG1()) == false || model.isNumber(viewLabor.getBSG2()) == false )
			{
				JOptionPane.showMessageDialog(null, "Es befinden sich unerlaubte Zeichen in den Feldern!");
			}
			
			else if(model.isOutOfRange(viewLabor.getBSG1(), 0, 150) || model.isOutOfRange(viewLabor.getBSG2(), 0, 150))
			{
				
				JOptionPane.showMessageDialog(null, "BSG ausserhalb des m�glichen Bereiches!");
			}
			
			else if(model.secondSmallerThanFirst(viewLabor.getBSG1(), viewLabor.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "1. BSG ist h�her als 2. BSG!");
				
			}
			
			else
			{
				
				if(model.isOutOfRange(viewLabor.getCreatinin(), 40, 110))
				{
					JOptionPane.showMessageDialog(null, "!ACHTUNG!" + "\n" + "Creatinin liegt ausserhalb des physiologischen Messbereiches!");
				}
			
			
			
			
			Class_Labor_Aufnahme labA	= new Class_Labor_Aufnahme(viewLabor.getID(), 
					viewLabor.getDatumLaborAufnahme(), 
					viewLabor.getCholesterin(), 
					viewLabor.getCreatinin(), 
					viewLabor.getTriglyceride(),
					viewLabor.getBSG1(), 
					viewLabor.getBSG2());
			
			
			
			viewLabor.setVisible(false);
			
			viewExpo.setVisible(true);
			}
			
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
