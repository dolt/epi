package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import classes.Class_Expo_Aufnahme;
import database.Query;
import model.Model;
import view.View_Expo_Aufnahme;
import view.View_Labor_Aufnahme;
import view.View_Menu;
import view.View_Stammdaten;

public class Controller_Expo_Aufnahme {
	
	private Model model;
	private View_Expo_Aufnahme 	viewExpo;
	private View_Stammdaten		viewStamm;
	private View_Labor_Aufnahme	viewLabor;
	private View_Menu			viewMenu;
	
	
	public Controller_Expo_Aufnahme(Model model, View_Expo_Aufnahme viewExpo, View_Stammdaten viewStamm, View_Labor_Aufnahme viewLabor, View_Menu viewMenu)
	{
		
		this.model		= model;
		this.viewExpo 	= viewExpo;
		this.viewStamm	= viewStamm;
		this.viewLabor	= viewLabor;
		this.viewMenu	= viewMenu;
		
		
		
		viewExpo.addExpoWeiterButtonListener(new ExpoWeiterButtonListener());
		viewExpo.addExpoZurückButtonListener(new ExpoZurückButtonListener());
		viewExpo.addRauchRadioButtonListener(new RauchRadioButtonListener());
		viewExpo.addRauchEhemalsRadioButtonListener(new RauchEhemalsRadioButtonListener());
		viewExpo.addAlkNieRadioListener(new AlkNieRadioListener());
		viewExpo.addAlkGelegentlichRadioListener(new AlkGelegentlichRadioListener());
		viewExpo.addAlkTäglichRadioListener(new AlkTäglichRadioListener());
		//viewExpo.addButtonVerlassenUndSpeichern(new VerlassenUndSpeichernListener());
		
		
	}
	
	//momentan nicht aktiv...
	
	public class VerlassenUndSpeichernListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			
		}	
	}
	
	
	public class AlkNieRadioListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			viewExpo.deactivateDrinkingFields();
			
			
		}
		
	}
	
	public class AlkGelegentlichRadioListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			viewExpo.activateDrinkingFields();
			
		}
		
	}
	
	public class AlkTäglichRadioListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			viewExpo.activateDrinkingFields();
			
		}
		
	}
	
	
	
	
	public class RauchEhemalsRadioButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			viewExpo.activateSmokingFields();
			
		}
		
	
	}
	
	
	
	public class RauchRadioButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
		
			viewExpo.deactivateSmokingFields();
			
		}
		
	}
	
	
	
	public class ExpoWeiterButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			if(model.isNull(viewExpo.getDatumExpoAufnahme()) ||model.isNull(viewExpo.getAlterStartRauchen()) || model.isNull(viewExpo.getAnzahlJahreNichtraucher()) || model.isNull(viewExpo.getAnzahlZigaretten()) || model.isNull(viewExpo.getAlkoholProTag()))
			{
				
				JOptionPane.showMessageDialog(null, "Bitte füllen Sie alle Felder aus!");
			}
			
			
			
			else if(model.isNumber(viewExpo.getAlterStartRauchen()) == false || model.isNumber(viewExpo.getAnzahlJahreNichtraucher()) == false || model.isNumber(viewExpo.getAnzahlZigaretten()) == false || model.isNumber(viewExpo.getAlkoholProTag()) == false )
			{
				
				JOptionPane.showMessageDialog(null, "Es befindet sich nicht erlaubte Zeichen in den Feldern!");
				
			}
			
			
			else
			{
				
				
				Class_Expo_Aufnahme expoA	= new Class_Expo_Aufnahme(viewExpo.getID(), 
																	viewExpo.getDatumExpoAufnahme(), 
																	viewExpo.getRauchen(), 
																	viewExpo.getRauchArt(), 
																	viewExpo.getAlterStartRauchen(), 
																	viewExpo.getAnzahlJahreNichtraucher(), 
																	viewExpo.getAnzahlZigaretten(), 
																	viewExpo.getAlkFrequenz(), 
																	viewExpo.getAlkoholProTag(), 
																	viewExpo.getGesundheitlicheBedenken());
				
				
				
				
			
			
			viewExpo.setVisible(false);
			viewLabor.setVisible(true);
			viewLabor.setLocationRelativeTo(null);
			
			}
		}
		

	}
	
	
	public class ExpoZurückButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			
			Class_Expo_Aufnahme expoA	= new Class_Expo_Aufnahme(viewExpo.getID(), 
					viewExpo.getDatumExpoAufnahme(), 
					viewExpo.getRauchen(), 
					viewExpo.getRauchArt(), 
					viewExpo.getAlterStartRauchen(), 
					viewExpo.getAnzahlJahreNichtraucher(), 
					viewExpo.getAnzahlZigaretten(), 
					viewExpo.getAlkFrequenz(), 
					viewExpo.getAlkoholProTag(), 
					viewExpo.getGesundheitlicheBedenken());
			
			
			
			viewExpo.setVisible(false);
			viewStamm.setVisible(true);
			viewStamm.setLocationRelativeTo(null);
			
			
		}
		
	}
	

}
