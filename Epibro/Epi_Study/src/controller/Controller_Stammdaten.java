package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;











import javax.swing.text.ViewFactory;

import classes.Class_Expo_Aufnahme;
import classes.Class_Labor_Aufnahme;
import classes.Class_Labor_Untersuchung;
import classes.Class_Stammdaten;
import classes.OwnClass_BodyCheck;
import classes.OwnClass_FollwoUP;
import database.Query;
import model.Model;
import view.View_BodyCheck;
import view.View_Expo_Aufnahme;
import view.View_FollowUP;
import view.View_Labor_Aufnahme;
import view.View_Labor_Untersuchung;
import view.View_Menu;
import view.View_Stammdaten;

public class Controller_Stammdaten {
	
	
	private Model 				model;
	private View_Stammdaten 	viewStamm;
	private View_Expo_Aufnahme 	viewExpo;
	private View_Menu			viewMenu;
	private View_Labor_Aufnahme	viewLabA;
	private View_Labor_Untersuchung	viewLabU;
	private View_FollowUP		viewFollow;
	private View_BodyCheck		viewBody;
	
	public Controller_Stammdaten(Model model, View_Stammdaten viewStamm, View_Expo_Aufnahme viewExpo, View_Menu viewMenu, View_Labor_Aufnahme	viewLabA, View_Labor_Untersuchung viewLabU, View_FollowUP viewFollow, View_BodyCheck viewBody)
	{
		
		this.model 		= model;
		this.viewStamm	= viewStamm;
		this.viewExpo	= viewExpo;
		this.viewMenu	= viewMenu;
		this.viewLabA	= viewLabA;
		this.viewLabU	= viewLabU;
		this.viewFollow	= viewFollow;
		this.viewBody	= viewBody;
		
		
		viewStamm.addWeiterButtonListener(new WeiterButtonListener());
		viewStamm.addMen�ButtonListener(new Men�ButtonListener());
		
		
	}
	
	
	
	
	
	public class WeiterButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			
			
			/***
			 * Wird auf 'Weiter' geklickt, dann wird sofort ein Plausibilit�tscheck durchgef�hrt und die Daten in die 'Klasse' gezogen und dort gespeichert bzw �berschrieben 
			 * OHNE sie in die DaBa zu speichern
			 * 
			 * */
			
			
			
			//Plausibilit�tschecks....
			
			
			//...auf "leere Felder"....
			
			if(model.isNull(viewStamm.getAufnahmeDatum()) || model.isNull(viewStamm.getZentrum()) || model.isNull(viewStamm.getGeburtsDatum()) || model.isNull(viewStamm.getGewicht()) || model.isNull(viewStamm.getGroesse()))
			{
				
				JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Felder aus!");
				
				
			}
			
			//...auf Zahlen / unerlaubte Zeichen....
			
			else if(model.isNumber(viewStamm.getGewicht())  == false)
			{
				
				JOptionPane.showMessageDialog(null, "Im Feld 'Gewicht' befindet sich nicht erlaubte Zeichen!");
				
			}
			
			else if( model.isNumber(viewStamm.getGroesse()) == false)
			{
				
				
				JOptionPane.showMessageDialog(null, "Im Feld 'Groesse' befindet sich nicht erlaubte Zeichen!");
				
			}
			
			
			else
			{
				
				//....und Range...
				
				
				if(model.isOutOfRange(viewStamm.getGroesse(), 100 , 250))
				{
					
					JOptionPane.showMessageDialog(null, "Gr��e des Patienten ist nicht plausibel!");
					
				}
				else if(model.isOutOfRange(viewStamm.getGewicht(), 30, 350))
				{
					
					JOptionPane.showMessageDialog(null, "Gewicht des Patienten ist nicht plausibel!");
					
				}
				
				else{
					
				
					
					//speicher die eingegebenen Daten in die Klasse Stammdaten....
					
					Class_Stammdaten stamm	= new Class_Stammdaten(viewStamm.getTF_Id(), 
							viewStamm.getAufnahmeDatum(), 
							viewStamm.getZentrum(), 
							viewStamm.getGeburtsDatum(),
							viewStamm.getGroesse(), 
							viewStamm.getGewicht());
					
					
					
					
					//Hier sollte hin: wenn 'bearbeiten', dann in der n�chsten view die werte in die Felder laden
					
					
					if(viewMenu.getUmwasgehts().equals("bearbeiten"))
					{
						
						String fall_id = viewExpo.getID();
						Class_Expo_Aufnahme expo = new Class_Expo_Aufnahme(fall_id, null, null, null, null, null, null, null, null, null);
						
						Query.loadExpoAufnahme(fall_id, expo);
						
						
						
					}
										
					
					
					
					
					viewStamm.setVisible(false);
					viewExpo.setVisible(true);
					viewExpo.setLocationRelativeTo(null);
			
				}
			
			}
		}
			
	}
	
	
	public class Men�ButtonListener implements ActionListener
	{

		
		@Override
		public void actionPerformed(ActionEvent e) {
			
		
			/***
			 * Stammdaten:
			 * 
			 * wird auf 'Men�' geklickt, dann wird zuerst �berpr�ft ob ein Datum vorhanden ist. Ist dies nicht der fall, wird abgebrochen und ins Men� zur�ck gegangen 
			 * 
			 * Ist ein Datum vorhanden, und es wird auf 'Men�' geklickt, werden alle Daten aus den Textfeldern in die 'Klasse' gezogen, an die Datenbank Methode �bergeben
			 * und in der datenbank gespeichert
			 * 
			 *
			 */
			
			
			//#########################View Stammdaten speichern####################
			
			
			
			if(model.isNull(viewStamm.getAufnahmeDatum()))
			{
				
				JOptionPane.showMessageDialog(null, "Daten werden nicht gespeichert!");
				viewStamm.setVisible(false);
				viewMenu.setVisible(true);
				viewMenu.setLocationRelativeTo(null);
			}
			
			
			else
			{
				
				
				//Plausibilit�tschecks....
				
				
				//...auf "leere Felder"....
				
				if(model.isNull(viewStamm.getAufnahmeDatum()) || model.isNull(viewStamm.getZentrum()) || model.isNull(viewStamm.getGeburtsDatum()) || model.isNull(viewStamm.getGewicht()) || model.isNull(viewStamm.getGroesse()))
				{
					
					JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Felder aus!");
					
					
				}
				
				//...auf Zahlen / unerlaubte Zeichen....
				
				else if(model.isNumber(viewStamm.getGewicht())  == false)
				{
					
					JOptionPane.showMessageDialog(null, "Im Feld 'Gewicht' befindet sich nicht erlaubte Zeichen!");
					
				}
				
				else if( model.isNumber(viewStamm.getGroesse()) == false)
				{
					
					
					JOptionPane.showMessageDialog(null, "Im Feld 'Groesse' befindet sich nicht erlaubte Zeichen!");
					
				}
				
				
				else
				{
					
					//....und Range...
					
					
					if(model.isOutOfRange(viewStamm.getGroesse(), 100 , 250))
					{
						
						JOptionPane.showMessageDialog(null, "Gr��e des Patienten ist nicht plausibel!");
						
					}
					else if(model.isOutOfRange(viewStamm.getGewicht(), 30, 350))
					{
						
						JOptionPane.showMessageDialog(null, "Gewicht des Patienten ist nicht plausibel!");
						
					}
				
				
			
				
				try{
					
				
						//speicher die eingegebenen Daten in die Klasse Stammdaten....
						
						Class_Stammdaten stamm	= new Class_Stammdaten(viewStamm.getTF_Id(), 
								viewStamm.getAufnahmeDatum(), 
								viewStamm.getZentrum(), 
								viewStamm.getGeburtsDatum(),
								viewStamm.getGroesse(), 
								viewStamm.getGewicht());
				
				
						Query.storeStammdaten(stamm.getPat_id(), stamm.getDatum_stammdat(), stamm.getZentrum_stammdat(), stamm.getGeburt_stammdat(), stamm.getGroesse_cm_stammdat(), stamm.getGewicht_kg_stammdat(), viewMenu.getUserID());
					
						
						//alles wieder leeren nach dem Speichern in die DaBa
						
						stamm.setPat_id(null);
						stamm.setDatum_stammdat(null);
						stamm.setZentrum_stammdat(null);
						stamm.setGeburt_stammdat(null);
						stamm.setGroesse_cm_stammdat(null);
						stamm.setGewicht_kg_stammdat(null);
						
						
						
						
						
						
						
				
				}catch(Exception ex)
				{
					
					JOptionPane.showMessageDialog(null, "Speichern nicht erfolgreich -> Controller Stammdaten");
					
				}
						
						
						
				//#########################View Expo speichern####################
				//Plausie Check nicht n�tig, da es bei weiter/zur�ck bereits gecheckt wird		
						
				
					
					
					Class_Expo_Aufnahme expoA	= new Class_Expo_Aufnahme(viewExpo.getID(), 
																		viewExpo.getDatumExpoAufnahme(), 
																		viewExpo.getRauchen(), 
																		viewExpo.getRauchArt(), 
																		viewExpo.getAlterStartRauchen(), 
																		viewExpo.getAnzahlJahreNichtraucher(), 
																		viewExpo.getAnzahlZigaretten(), 
																		viewExpo.getAlkFrequenz(), 
																		viewExpo.getAlkoholProTag(), 
																		viewExpo.getGesundheitlicheBedenken());
					
					
					
					Query.storeExposition(viewMenu.getFallID(), expoA.getDatum_expo_aufnahme(), expoA.getRauchen(), expoA.getRauchenArt(), expoA.getRauchenBeginn(), expoA.getRauchenEnde(), expoA.getRauchenAnzahl(), expoA.getAlkoholH�ufigkeit(), expoA.getAlkoholGramm(), expoA.getGesundheitlicheBedenken(), viewMenu.getUserID());
					
					
					
					//alles wieder leeren nach dem Speichern in die DaBa
					
					expoA.setPat_id(null);
					expoA.setAlkoholGramm(null);
					expoA.setAlkoholH�ufigkeit(null);
					expoA.setDatum_expo_aufnahme(null);
					expoA.setGesundheitlicheBedenken(null);
					expoA.setRauchen(null);
					expoA.setRauchenAnzahl(null);
					expoA.setRauchenArt(null);
					expoA.setRauchenBeginn(null);
					expoA.setRauchenEnde(null);
					
					
					
					
					
					
					//#########################View Labor Aufnahme speichern####################
					//Plausie Check nicht n�tig, da es bei weiter/zur�ck bereits gecheckt wird
					
					
					
					Class_Labor_Aufnahme labA	= new Class_Labor_Aufnahme(viewLabA.getID(), 
																			viewLabA.getDatumLaborAufnahme(),
																			viewLabA.getCholesterin(), 
																			viewLabA.getCreatinin(), 
																			viewLabA.getTriglyceride(), 
																			viewLabA.getBSG1(), 
																			viewLabA.getBSG2());
					
					Query.storeLabor("aufnahme", viewMenu.getFallID(), labA.getDatum_labor_aufnahme(), labA.getCholesterin_aufnahme(), labA.getCreatinin_aufnahme(), labA.getTriglyceride_aufnahme(), labA.getBsg1_aufnahme(), labA.getBsg2_aufnahme(), viewMenu.getUserID());
					
					//alles wieder leeren nach dem Speichern in die DaBa
					labA.setPat_id(null);
					labA.setDatum_labor_aufnahme(null);
					labA.setCholesterin_aufnahme(null);
					labA.setCreatinin_aufnahme(null);
					labA.setTriglyceride_aufnahme(null);
					labA.setBsg1_aufnahme(null);
					labA.setBsg2_aufnahme(null);
					
					
					
					
					
					//#########################View Labor Untersuchung speichern####################
					//Plausie Check nicht n�tig, da es bei weiter/zur�ck bereits gecheckt wird
					
					
					
					Class_Labor_Untersuchung labU	= new Class_Labor_Untersuchung(viewLabU.getID(), 
							viewLabU.getDatumLaborAufnahme(),
							viewLabU.getCholesterin(), 
							viewLabU.getCreatinin(), 
							viewLabU.getTriglyceride(), 
							viewLabU.getBSG1(), 
							viewLabU.getBSG2());

					
					
					
					Query.storeLabor("untersuchung", viewMenu.getFallID(), labU.getDatum_labor_untersuchung(), labU.getCholesterin_untersuchung(), labU.getCreatinin_untersuchung(), labU.getTriglyceride_Untersuchung(), labU.getBsg1_untersuchung(), labU.getBsg2_untersuchung(), viewMenu.getUserID());

					
					
					//alles wieder leeren nach dem Speichern in die DaBa
					labU.setPat_id(null);
					labU.setDatum_labor_untersuchung(null);
					labU.setCholesterin_untersuchung(null);
					labU.setCreatinin_untersuchung(null);
					labU.setTriglyceride_Untersuchung(null);
					labU.setBsg1_untersuchung(null);
					labU.setBsg2_untersuchung(null);
					
					
					
					//######################Bodycheck speichern#########################
					
					
					OwnClass_BodyCheck body	= new OwnClass_BodyCheck(viewBody.getTf_ID(),
																	viewBody.getDateChooser(), 
																	model.booleantoSTring(viewBody.getRdbtnDieag_Y()),
																	viewBody.getTextFieldsystol(), 
																	viewBody.getTextField_dia(), 
																	viewBody.getTextField_puls());
					
					
					Query.storeBodycheck(viewMenu.getFallID(), body.getDate(), body.getHypertonieYN(), body.getSyssto(), body.getDiasto(), body.getPuls(), viewMenu.getUserID());
					
					body.setDate(null);
					body.setDiasto(null);
					body.setHypertonieYN(null);
					body.setID(null);
					body.setPuls(null);
					body.setSyssto(null);

					
					
					
				
					
					
					//#########################View Follow up speichern####################
					//Plausie Check nicht n�tig, da es bei weiter/zur�ck bereits gecheckt wird
					
					OwnClass_FollwoUP followUp	= new OwnClass_FollwoUP(viewFollow.getTf_ID(), 
																		viewFollow.getDatumFollowUp(), 
																		model.booleantoSTring(viewFollow.getRdbtn_Infarkt_Y()), 
																		model.booleantoSTring(viewFollow.getRdbtn_Died_Y()), 
																		viewFollow.getDatumInfarkt());
					
					
					Query.storeFollowUp(viewMenu.getFallID(), followUp.getDate(), followUp.getInfarktYN(), followUp.isDiedyetYN(), followUp.getInfarktDate(), viewMenu.getUserID());
					
					followUp.setDate(null);
					followUp.setDiedyetYN(null);
					followUp.setID(null);
					followUp.setInfarktDate(null);
					followUp.setInfarktYN(null);
				
					
					
					
					
					
					
					

					
					viewStamm.setVisible(false);
			
				}
							
				
			}
				
		}
				
		
	}
	
}

	
	
	
	
	
	
	


	


