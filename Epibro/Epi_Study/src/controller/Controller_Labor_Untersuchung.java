package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import classes.Class_Labor_Untersuchung;
import model.Model;
import view.View_BodyCheck;
import view.View_Expo_Aufnahme;
import view.View_FollowUP;
import view.View_Labor_Aufnahme;
import view.View_Labor_Untersuchung;

public class Controller_Labor_Untersuchung {
	
	
	private Model model;
	private View_Labor_Aufnahme		viewLabor;
	private View_Labor_Untersuchung	viewLaborU;
	private View_FollowUP			viewFollow;
	private View_BodyCheck			viewBody;
	
	public Controller_Labor_Untersuchung(Model model, View_Labor_Aufnahme viewLabor, View_Labor_Untersuchung viewLaborU, View_FollowUP viewFollow, View_BodyCheck viewBody)
	{
		
		this.model		= model;
		this.viewLabor	= viewLabor;
		this.viewLaborU	= viewLaborU;
		this.viewFollow	= viewFollow;
		this.viewBody	= viewBody;
		
		
		viewLaborU.addLaborWeiterButtonListener(new LaborWeiterButtonListener());
		viewLaborU.addLaborZur�ckButtonListener(new LaborZur�ckButtonListener());
		
		
	}
	
	
	public class LaborWeiterButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
		
			
			if(model.isNull(viewLaborU.getDatumLaborAufnahme()) || model.isNull(viewLaborU.getCholesterin()) || model.isNull(viewLaborU.getCholesterin()) || model.isNull(viewLaborU.getTriglyceride()) || model.isNull(viewLaborU.getBSG1()) || model.isNull(viewLaborU.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Textfelder aus!");
			}
			
			else if(model.isNumber(viewLaborU.getCholesterin()) == false || model.isNumber(viewLaborU.getCholesterin()) == false || model.isNumber(viewLaborU.getTriglyceride()) == false || model.isNumber(viewLaborU.getBSG1()) == false || model.isNumber(viewLaborU.getBSG2()) == false )
			{
				JOptionPane.showMessageDialog(null, "Es befinden sich unerlaubte Zeichen in den Feldern!");
			}
			
			else if(model.isOutOfRange(viewLaborU.getBSG1(), 0, 150) || model.isOutOfRange(viewLaborU.getBSG2(), 0, 150))
			{
				
				JOptionPane.showMessageDialog(null, "BSG ausserhalb des m�glichen Bereiches!");
			}
			
			else if(model.secondSmallerThanFirst(viewLaborU.getBSG1(), viewLaborU.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "1. BSG ist h�her als 2. BSG!");
				
			}
			
			else
			{
				
				if(model.isOutOfRange(viewLaborU.getCreatinin(), 40, 110))
				{
					JOptionPane.showMessageDialog(null, "!ACHTUNG!" + "\n" + "Creatinin liegt ausserhalb des physiologischen Messbereiches!");
				}
				
				
				viewLaborU.setVisible(false);
				viewBody.setLocationRelativeTo(null);
				viewBody.setVisible(true);
				
				
				Class_Labor_Untersuchung labU	= new Class_Labor_Untersuchung(viewLaborU.getID(), 
																				viewLaborU.getDatumLaborAufnahme(), 
																				viewLaborU.getCholesterin(), 
																				viewLaborU.getCreatinin(),
																				viewLaborU.getTriglyceride(), 
																				viewLaborU.getBSG1(), 
																				viewLaborU.getBSG2());
				
				
				
				
			}
			
			
			
			
			
		}
		
	}
	
	public class LaborZur�ckButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			if(model.isNull(viewLaborU.getDatumLaborAufnahme()) || model.isNull(viewLaborU.getCholesterin()) || model.isNull(viewLaborU.getCholesterin()) || model.isNull(viewLaborU.getTriglyceride()) || model.isNull(viewLaborU.getBSG1()) || model.isNull(viewLaborU.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Textfelder aus!");
			}
			
			else if(model.isNumber(viewLaborU.getCholesterin()) == false || model.isNumber(viewLaborU.getCholesterin()) == false || model.isNumber(viewLaborU.getTriglyceride()) == false || model.isNumber(viewLaborU.getBSG1()) == false || model.isNumber(viewLaborU.getBSG2()) == false )
			{
				JOptionPane.showMessageDialog(null, "Es befinden sich unerlaubte Zeichen in den Feldern!");
			}
			
			else if(model.isOutOfRange(viewLaborU.getBSG1(), 0, 150) || model.isOutOfRange(viewLaborU.getBSG2(), 0, 150))
			{
				
				JOptionPane.showMessageDialog(null, "BSG ausserhalb des m�glichen Bereiches!");
			}
			
			else if(model.secondSmallerThanFirst(viewLaborU.getBSG1(), viewLaborU.getBSG2()))
			{
				JOptionPane.showMessageDialog(null, "1. BSG ist h�her als 2. BSG!");
				
			}
			
			else
			{
				
				if(model.isOutOfRange(viewLaborU.getCreatinin(), 40, 110))
				{
					JOptionPane.showMessageDialog(null, "!ACHTUNG!" + "\n" + "Creatinin liegt ausserhalb des physiologischen Messbereiches!");
				}
				
				
				viewLaborU.setVisible(false);
				viewFollow.setLocationRelativeTo(null);
				viewFollow.setVisible(true);
				
				
				Class_Labor_Untersuchung labU	= new Class_Labor_Untersuchung(viewLaborU.getID(), 
																				viewLaborU.getDatumLaborAufnahme(), 
																				viewLaborU.getCholesterin(), 
																				viewLaborU.getCreatinin(),
																				viewLaborU.getTriglyceride(), 
																				viewLaborU.getBSG1(), 
																				viewLaborU.getBSG2());
			
			
			
			viewLaborU.setVisible(false);
			
			viewLabor.setVisible(true);
			
			
			}
		
		}
	}

}
