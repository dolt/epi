package controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import classes.OwnClass_FollwoUP;
import view.View_BodyCheck;
import view.View_FollowUP;
import view.View_Labor_Untersuchung;
import model.Model;

public class Controller_FollowUp {

	private Model model;
	private View_FollowUP viewFollowUP;
	private View_BodyCheck view_Bodycheck;
	private View_Labor_Untersuchung viewLabU;
	

	// StorageFactory storagerfac = new StorageFactory();
	// StorageInterface interfaceDB = storagerfac.getStorage();

	public Controller_FollowUp(View_FollowUP viewFollowUP,View_BodyCheck viewBodycheck, Model model, View_Labor_Untersuchung viewLabU) {
		this.model = model;
		this.viewFollowUP = viewFollowUP;
		this.view_Bodycheck = viewBodycheck;
		this.viewLabU		= viewLabU;
		
		// viewEditMember.addMinusButtonListener(new MinusButtonListener());
		
		viewFollowUP.addBackButtonListener(new BackButtonListener());
		viewFollowUP.addDiedYet_NButtonListener(new DiedYetNButtonListener());
		viewFollowUP.addDiedYet_YButtonListener(new DiedYetYButtonListener());
		viewFollowUP.addrdbtn_Infarkt_NButtonListener(new InfarktNButtonListener());
		viewFollowUP.addrdbtn_Infarkt_YButtonListener(new InfarktYButtonListener());

	}
	

	public class InfarktNButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			viewFollowUP.hideInfaktDate();
		
		}

	}

	public class InfarktYButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			 viewFollowUP.showInfaktDate();

		}

	}

	public class DiedYetNButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			// TODO Auto-generated method stub

		}

	}

	public class DiedYetYButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

		}

	}

	public class FwardButtonListner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
				if((viewFollowUP.getRdbtn_Died_N()||viewFollowUP.getRdbtn_Died_Y())&&(viewFollowUP.getRdbtn_Infarkt_N()||viewFollowUP.getRdbtn_Infarkt_Y())){	
					
					OwnClass_FollwoUP newFollowUP = new OwnClass_FollwoUP(null, null, null, null, ".");
					
					newFollowUP.setDate(viewFollowUP.getDatumFollowUp());
					newFollowUP.setDiedyetYN(model.booleantoSTring(viewFollowUP.getRdbtn_Died_Y()));
					newFollowUP.setInfarktYN(model.booleantoSTring(viewFollowUP.getRdbtn_Infarkt_Y()));
					newFollowUP.setInfarktDate(viewFollowUP.getDatumInfarkt());
					
					view_Bodycheck.setVisible(true);
					viewFollowUP.setVisible(false);
					viewFollowUP.setLocationRelativeTo(null);
					
				
				}
				else
					JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Felder aus!");

		}
	}

	public class BackButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			OwnClass_FollwoUP newFollowUP = new OwnClass_FollwoUP(null, null, null, null, ".");
			
			newFollowUP.setDate(viewFollowUP.getDatumFollowUp());
			newFollowUP.setDiedyetYN(model.booleantoSTring(viewFollowUP.getRdbtn_Died_Y()));
			newFollowUP.setInfarktYN(model.booleantoSTring(viewFollowUP.getRdbtn_Infarkt_Y()));
			newFollowUP.setInfarktDate(viewFollowUP.getDatumInfarkt());
			
			System.out.println("zur�ck");
			
			viewFollowUP.setVisible(false);
			view_Bodycheck.setVisible(true);
			view_Bodycheck.setLocationRelativeTo(null);
			
			
			
		

		}
	}

}
