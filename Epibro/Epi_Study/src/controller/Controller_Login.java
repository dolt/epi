package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import database.DBConnection;
import database.Query;
import model.Model;
import view.View_Login;
import view.View_Menu;

public class Controller_Login {
	
	
	private Model model;
	private View_Login viewLog;
	private View_Menu viewMenu;
	
	
	
	public Controller_Login(Model model, View_Login viewLog, View_Menu viewMenu)
	{
		this.model = model;
		this.viewLog = viewLog;
		this.viewMenu = viewMenu;
		
		
		
		viewLog.addLoginButtonActionListener(new LoginButtonListener());
		
		
		
	}
	
	
	public class LoginButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			if(model.isNull(viewLog.getUsername()) || model.isNull(viewLog.getPassword())){
				
				JOptionPane.showMessageDialog(null, "Bitte alle felder ausf�llen!");
			}
			
			else
			{
				
				String id = Query.isLoginValid(viewLog.getUsername(), viewLog.getPassword());
				
				
				if(id!=null)
				{
					
					viewLog.dispose();
					
					
					
					
					viewMenu.setUserID(id); //User ID aus Datenbank erhalten
					
					
					
					
					viewMenu.fillComboBoxProject(Query.alleFaelleComboBox()); //alle F�lle in ComboBox rein
					
					
					viewMenu.setVisible(true);
					viewMenu.setLocationRelativeTo(null);	
				}
				
			}
			
		}
	
	}

}
