package main;

import java.text.DateFormat;
import java.util.GregorianCalendar;

import controller.Controller_BodyCheck;
import controller.Controller_Expo_Aufnahme;
import controller.Controller_FollowUp;
import controller.Controller_Labor_Aufnahme;
import controller.Controller_Labor_Untersuchung;
import controller.Controller_Login;
import controller.Controller_Menu;
import controller.Controller_Stammdaten;
import model.Model;
import view.View_BodyCheck;
import view.View_Expo_Aufnahme;
import view.View_FollowUP;
import view.View_Labor_Aufnahme;
import view.View_Labor_Untersuchung;
import view.View_Login;
import view.View_Menu;
import view.View_Stammdaten;

public class Main 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		
		Model						model			= new Model();
		
		View_Stammdaten 			viewStamm		= new View_Stammdaten(model);
		View_Expo_Aufnahme			viewExpo		= new View_Expo_Aufnahme(model);
		View_Labor_Aufnahme			viewLabor		= new View_Labor_Aufnahme(model);
		View_Labor_Untersuchung		viewLaborU		= new View_Labor_Untersuchung(model);
		View_Menu					viewMenu		= new View_Menu();
		View_Login					viewLog			= new View_Login();
		View_FollowUP				viewFollowUP	= new View_FollowUP(model);
		View_BodyCheck				viewBodycheck	= new View_BodyCheck(model);
		
		viewLog.setVisible(true);
		viewLog.setLocationRelativeTo(null);
		
		
		Controller_Login				contrLog		= new Controller_Login(model, viewLog, viewMenu);
		
		Controller_Menu					contrMenu		= new Controller_Menu(viewStamm, viewMenu, model, viewLabor, viewLaborU, viewExpo, viewFollowUP, viewBodycheck);
		
		Controller_Stammdaten 			contrStamm		= new Controller_Stammdaten(model, viewStamm, viewExpo, viewMenu, viewLabor, viewLaborU, viewFollowUP, viewBodycheck);
		
		Controller_Expo_Aufnahme 		contrExpo		= new Controller_Expo_Aufnahme(model, viewExpo, viewStamm, viewLabor, viewMenu);
		
		Controller_Labor_Aufnahme 		contrLabor		= new Controller_Labor_Aufnahme(model, viewExpo, viewLabor, viewLaborU);
		Controller_Labor_Untersuchung	contrLaborU		= new Controller_Labor_Untersuchung(model, viewLabor, viewLaborU, viewFollowUP, viewBodycheck);
		Controller_FollowUp				contrFollow		= new Controller_FollowUp(viewFollowUP, viewBodycheck, model, viewLaborU);
		Controller_BodyCheck			contrBody		= new Controller_BodyCheck(viewFollowUP, viewBodycheck, model, viewLaborU);
		
	}

}
