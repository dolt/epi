package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

import com.toedter.calendar.JDateChooser;

import model.Model;

public class View_Expo_Aufnahme extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField tf_ID;

	private ButtonGroup group_rauchen;
	private ButtonGroup group_bedenken;
	private ButtonGroup group_alkohol;
	
	
	private JCheckBox checkbox_Zigaretten;
	private JCheckBox checkbox_Pfeife;
	
	private JRadioButton radio_nie;
	private JRadioButton radio_ehemals;
	
	
	private JRadioButton radio_keine;
	private JRadioButton radio_unterVoraussetzungen;
	private JRadioButton radio_ja;
	
	
	private JRadioButton radio_alk_nie;
	private JRadioButton radio_alk_gelegentlich;
	private JRadioButton radio_alk_t�glich;
	
	private JButton button_zur�ck;
	private JButton button_weiter;
	
	
	private JTextField tf_alterStartRauchen;
	private JTextField tf_JahreNichtraucher;
	private JTextField tf_AlkoholProTag;
	private JTextField tf_ZigarettenAnzahl;
	
	private Model model;
	
	private JButton btnVerlassenUndSpeichern;
	
	
	private LineBorder border;
	
	private JDateChooser dateChooser_DatumExpoAufnahme;
	
	
	
	/**
	 * Create the dialog.
	 */
	public View_Expo_Aufnahme(Model model) {
		
		
		this.model	= model;
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		
		setResizable(false);
		setBounds(100, 100, 1090, 960);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("ID:");
		label.setFont(new Font("Arial", Font.BOLD, 12));
		label.setBounds(44, 39, 23, 14);
		contentPanel.add(label);
		
		tf_ID = new JTextField(6);
		tf_ID.setEditable(false);
		tf_ID.setBounds(76, 37, 86, 20);
		contentPanel.add(tf_ID);
		
		JLabel label_2 = new JLabel("Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		label_2.setFont(new Font("Verdana", Font.BOLD, 23));
		label_2.setBounds(247, 152, 587, 26);
		contentPanel.add(label_2);
		
		JLabel lblExpositionserhebungBeiAufnahme = new JLabel("Expositionserhebung bei Aufnahme");
		lblExpositionserhebungBeiAufnahme.setFont(new Font("Verdana", Font.BOLD, 15));
		lblExpositionserhebungBeiAufnahme.setBounds(331, 189, 345, 26);
		contentPanel.add(lblExpositionserhebungBeiAufnahme);
		
		JLabel lblDatum = new JLabel("Datum:");
		lblDatum.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDatum.setBounds(215, 255, 77, 14);
		contentPanel.add(lblDatum);
		
		JLabel lblHabenSieSchon = new JLabel("Haben Sie schon einmal geraucht oder rauchen Sie zur Zeit?");
		lblHabenSieSchon.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblHabenSieSchon.setBounds(215, 336, 306, 14);
		contentPanel.add(lblHabenSieSchon);
		
		radio_nie = new JRadioButton("nie");
		radio_nie.setBounds(570, 332, 46, 23);
		contentPanel.add(radio_nie);
		
		radio_ehemals = new JRadioButton("ehemalig");
		radio_ehemals.setBounds(644, 332, 109, 23);
		contentPanel.add(radio_ehemals);
		
		
		
		JLabel lblWasRauchenSie = new JLabel("Was rauchen Sie zur Zeit?");
		lblWasRauchenSie.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblWasRauchenSie.setBounds(286, 381, 143, 14);
		contentPanel.add(lblWasRauchenSie);
		
		JLabel lblWennJa = new JLabel("Wenn ja:");
		lblWennJa.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblWennJa.setBounds(215, 381, 63, 14);
		contentPanel.add(lblWennJa);
		
		checkbox_Pfeife = new JCheckBox("Pfeife");
		checkbox_Pfeife.setBounds(570, 377, 63, 23);
		contentPanel.add(checkbox_Pfeife);
		
		checkbox_Zigaretten = new JCheckBox("Zigaretten");
		checkbox_Zigaretten.setBounds(644, 377, 123, 23);
		contentPanel.add(checkbox_Zigaretten);
		
		JLabel lblFallsPfeifeOder = new JLabel("Falls Pfeife oder Zigaretten:");
		lblFallsPfeifeOder.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFallsPfeifeOder.setBounds(215, 430, 200, 14);
		contentPanel.add(lblFallsPfeifeOder);
		
		JLabel lblFallsEhemalig = new JLabel("Falls ehemalig:");
		lblFallsEhemalig.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFallsEhemalig.setBounds(215, 487, 97, 14);
		contentPanel.add(lblFallsEhemalig);
		
		JLabel lblFallsSieZigaretten = new JLabel("Falls Sie Zigaretten geraucht haben:");
		lblFallsSieZigaretten.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFallsSieZigaretten.setBounds(215, 539, 214, 14);
		contentPanel.add(lblFallsSieZigaretten);
		
		JLabel lblFallsSiederzeitAlkohol = new JLabel("Falls Sie derzeit Alkohol trinken:");
		lblFallsSiederzeitAlkohol.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFallsSiederzeitAlkohol.setBounds(215, 645, 193, 14);
		contentPanel.add(lblFallsSiederzeitAlkohol);
		
		JLabel lblHabenSieGesundheitliche = new JLabel("Haben Sie gesundheitliche Bedenken?");
		lblHabenSieGesundheitliche.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblHabenSieGesundheitliche.setBounds(215, 714, 306, 14);
		contentPanel.add(lblHabenSieGesundheitliche);
		
		JLabel lblInWelchemAlter = new JLabel("In welchem Alter wurde mit dem Rauchen begonnen?");
		lblInWelchemAlter.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblInWelchemAlter.setBounds(215, 441, 306, 14);
		contentPanel.add(lblInWelchemAlter);
		
		JLabel lblSeitWannSind = new JLabel("Seit wann sind Sie kein Raucher mehr?");
		lblSeitWannSind.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblSeitWannSind.setBounds(215, 499, 237, 14);
		contentPanel.add(lblSeitWannSind);
		
		JLabel lblMit = new JLabel("mit");
		lblMit.setBounds(537, 441, 23, 14);
		contentPanel.add(lblMit);
		
		JLabel lblJahren = new JLabel("Jahren");
		lblJahren.setBounds(626, 441, 46, 14);
		contentPanel.add(lblJahren);
		
		tf_alterStartRauchen = new JTextField(2);
		tf_alterStartRauchen.setBounds(570, 438, 46, 20);
		contentPanel.add(tf_alterStartRauchen);
		tf_alterStartRauchen.setColumns(10);
		
		JLabel lblSeit = new JLabel("seit");
		lblSeit.setBounds(537, 499, 23, 14);
		contentPanel.add(lblSeit);
		
		tf_JahreNichtraucher = new JTextField(2);
		tf_JahreNichtraucher.setColumns(10);
		tf_JahreNichtraucher.setBounds(570, 496, 46, 20);
		contentPanel.add(tf_JahreNichtraucher);
		
		JLabel label_3 = new JLabel("Jahren");
		label_3.setBounds(626, 499, 46, 14);
		contentPanel.add(label_3);
		
		JLabel lblWieVieleZigaretten = new JLabel("Wie viele Zigaretten wurden durchschnittlich pro Tag geraucht?");
		lblWieVieleZigaretten.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblWieVieleZigaretten.setBounds(215, 550, 327, 14);
		contentPanel.add(lblWieVieleZigaretten);
		
		JLabel lblWieVielGramm = new JLabel("Wie viel Gramm trinken Sie ungef\u00E4hr pro Tag?");
		lblWieVielGramm.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblWieVielGramm.setBounds(215, 658, 306, 14);
		contentPanel.add(lblWieVielGramm);
		
		JLabel lblBierml = new JLabel("(1 Bier (500ml) = 20g oder 1/4l Wein = 20g)");
		lblBierml.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblBierml.setBounds(215, 670, 306, 14);
		contentPanel.add(lblBierml);
		
		tf_AlkoholProTag = new JTextField(3);
		tf_AlkoholProTag.setBounds(570, 655, 46, 20);
		contentPanel.add(tf_AlkoholProTag);
		tf_AlkoholProTag.setColumns(10);
		
		JLabel lblG = new JLabel("g");
		lblG.setBounds(626, 658, 23, 14);
		contentPanel.add(lblG);
		
		radio_keine = new JRadioButton("keine");
		radio_keine.setBounds(570, 710, 63, 23);
		contentPanel.add(radio_keine);
		
		radio_unterVoraussetzungen = new JRadioButton("unter bestimmten Voraussetzungen");
		radio_unterVoraussetzungen.setBounds(570, 736, 231, 23);
		contentPanel.add(radio_unterVoraussetzungen);
		
	
		radio_ja = new JRadioButton("ja");
		radio_ja.setBounds(570, 762, 63, 23);
		contentPanel.add(radio_ja);
	
		
		
		
		tf_ZigarettenAnzahl = new JTextField(3);
		tf_ZigarettenAnzahl.setColumns(10);
		tf_ZigarettenAnzahl.setBounds(570, 547, 46, 20);
		contentPanel.add(tf_ZigarettenAnzahl);
		
		JLabel lblStck = new JLabel("St\u00FCck");
		lblStck.setBounds(626, 550, 46, 14);
		contentPanel.add(lblStck);
		
		JLabel lblWieOftTrinken = new JLabel("Wie oft trinken Sie");
		lblWieOftTrinken.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblWieOftTrinken.setBounds(215, 591, 103, 14);
		contentPanel.add(lblWieOftTrinken);
		
		JLabel lblAlkohol = new JLabel("Alkohol?");
		lblAlkohol.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblAlkohol.setBounds(357, 591, 103, 14);
		contentPanel.add(lblAlkohol);
		
		JLabel lblDerzeit = new JLabel("derzeit");
		lblDerzeit.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDerzeit.setBounds(307, 591, 63, 14);
		contentPanel.add(lblDerzeit);
		
		radio_alk_nie = new JRadioButton("nie");
		radio_alk_nie.setBounds(570, 587, 54, 23);
		contentPanel.add(radio_alk_nie);
		
		radio_alk_gelegentlich = new JRadioButton("gelegentlich");
		radio_alk_gelegentlich.setBounds(636, 587, 109, 23);
		contentPanel.add(radio_alk_gelegentlich);
		
		radio_alk_t�glich = new JRadioButton("t\u00E4glich");
		radio_alk_t�glich.setBounds(745, 587, 89, 23);
		contentPanel.add(radio_alk_t�glich);
		
		button_weiter = new JButton("weiter");
		button_weiter.setBounds(940, 858, 91, 23);
		contentPanel.add(button_weiter);
		
		button_zur�ck = new JButton("zur\u00FCck");
		button_zur�ck.setBounds(661, 858, 91, 23);
		contentPanel.add(button_zur�ck);
		
		JLabel label_1 = new JLabel("Herzinfarktverbund");
		label_1.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_1.setBounds(625, 18, 290, 51);
		contentPanel.add(label_1);
		
		JLabel label_4 = new JLabel("Baden-W\u00FCrttemberg");
		label_4.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_4.setBounds(741, 59, 290, 34);
		contentPanel.add(label_4);
		
		
		border = (LineBorder) BorderFactory.createLineBorder(Color.black);
		
		JLabel label_li = new JLabel("");
		label_li.setBorder(border);
		label_li.setBounds(170, 312, 362, 513);
		contentPanel.add(label_li);
		
		
		
		JLabel label_re = new JLabel("");
		label_re.setBorder(border);
		label_re.setBounds(531, 312, 362, 513);
		contentPanel.add(label_re);
		
		dateChooser_DatumExpoAufnahme = new JDateChooser();
		dateChooser_DatumExpoAufnahme.setBounds(281, 255, 109, 20);
		contentPanel.add(dateChooser_DatumExpoAufnahme);
		
		
		//Button groups erstellen - immer nur 1 davon kann angeklickt sein...
	
		
		group_rauchen	= new ButtonGroup();
		group_rauchen.add(radio_nie);
		group_rauchen.add(radio_ehemals);
		
		
		
		group_alkohol 	= new ButtonGroup();
		group_alkohol.add(radio_alk_nie);
		group_alkohol.add(radio_alk_gelegentlich);
		group_alkohol.add(radio_alk_t�glich);
		
		
	
		group_bedenken	= new ButtonGroup();
		group_bedenken.add(radio_keine);
		group_bedenken.add(radio_unterVoraussetzungen);
		group_bedenken.add(radio_ja);
		
		btnVerlassenUndSpeichern = new JButton("Verlassen+Speichern");
		btnVerlassenUndSpeichern.setBounds(780, 858, 135, 23);
		contentPanel.add(btnVerlassenUndSpeichern);
		
		
		
		
		
		
		
	}
	
	
	
	public void clearAll()
	{
		
		tf_ID.setText("");
		
		checkbox_Zigaretten.setSelected(false);
		checkbox_Pfeife.setSelected(false);
		
		group_alkohol.clearSelection();
		group_bedenken.clearSelection();
		group_rauchen.clearSelection();

		tf_alterStartRauchen.setText("");
		tf_JahreNichtraucher.setText("");
		tf_AlkoholProTag.setText("");
		tf_ZigarettenAnzahl.setText("");
		
		dateChooser_DatumExpoAufnahme.setDate(null);
		
	}
	
	
	
	
	public void setDatum(String datum) 
	{
		
		
		DateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMAN);
		try {
			Date date = format.parse(datum);
			dateChooser_DatumExpoAufnahme.setDate(date);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	/*public void addButtonVerlassenUndSpeichern(ActionListener al)
	{
		
		btnVerlassenUndSpeichern.addActionListener(al);
	}
	*/
	
	
	public void addExpoWeiterButtonListener(ActionListener al)
	{
		button_weiter.addActionListener(al);
	}

	
	public void addExpoZur�ckButtonListener(ActionListener al)
	{
		button_zur�ck.addActionListener(al);
	}
	
	
	
	

	public void addRauchRadioButtonListener(ActionListener al)
	{
		radio_nie.addActionListener(al);
		
	}
	
	public void addRauchEhemalsRadioButtonListener(ActionListener al)
	{
		radio_ehemals.addActionListener(al);
		
	}
	
	
	
	public void addAlkNieRadioListener(ActionListener al)
	{
		radio_alk_nie.addActionListener(al);
	}
	
	
	public void addAlkGelegentlichRadioListener(ActionListener al)
	{
		radio_alk_gelegentlich.addActionListener(al);
	}
	
	public void addAlkT�glichRadioListener(ActionListener al)
	{
		radio_alk_t�glich.addActionListener(al);
	}
	
	
	
	
	//Felder Inhalt bekommen
	
	
	public String getID()
	{
		return tf_ID.getText();
	}
	
	
	
	public String getDatumExpoAufnahme()
	{
		
		return ((JTextField)dateChooser_DatumExpoAufnahme.getDateEditor().getUiComponent()).getText(); 
	}
	
	public String getAlterStartRauchen()
	{
		return tf_alterStartRauchen.getText();
	}
	
	public String getAnzahlJahreNichtraucher()
	{
		return tf_JahreNichtraucher.getText();
	}
	
	public String getAnzahlZigaretten()
	{
		return tf_ZigarettenAnzahl.getText();
		
	}
	
	
	
	// ++++++++++++++++++++Radio Buttons und CheckBox++++++++
	
	
	public void deactivateSmokingFields()
	{
		
		tf_alterStartRauchen.setText(".");
		tf_alterStartRauchen.setEditable(false);
		tf_JahreNichtraucher.setText(".");
		tf_JahreNichtraucher.setEditable(false);
		tf_ZigarettenAnzahl.setText(".");
		tf_ZigarettenAnzahl.setEditable(false);
		
		checkbox_Pfeife.setEnabled(false);
		checkbox_Zigaretten.setEnabled(false);
		
	}
	
	
	public void activateSmokingFields()
	{
		tf_alterStartRauchen.setEditable(true);
		tf_alterStartRauchen.setText("");
		tf_JahreNichtraucher.setEditable(true);
		tf_JahreNichtraucher.setText("");
		tf_ZigarettenAnzahl.setEditable(true);
		tf_ZigarettenAnzahl.setText("");
		
		checkbox_Pfeife.setEnabled(true);
		checkbox_Zigaretten.setEnabled(true);
	}
	
	
	
	public void deactivateDrinkingFields()
	{
		tf_AlkoholProTag.setText(".");
		tf_AlkoholProTag.setEditable(false);
	}
	
	public void activateDrinkingFields()
	{
		
		tf_AlkoholProTag.setText("");
		tf_AlkoholProTag.setEditable(true);
	}
	
	
	//-------------------------------------------------------
	
	public String getRauchen()
	{
		
		String value = null;
		
		if(radio_nie.isSelected())
		{
			value = "nie";
		}
		else if(radio_ehemals.isSelected())
		{
			value = "ehemalig";
		}
		return value;
	}
	
	
	
	
	
	public String getRauchArt()
	{
		
		String value = null;
		
		if(checkbox_Pfeife.isSelected() && checkbox_Zigaretten.isSelected())
		{
			value = "Zigaretten, Pfeife";
		}
		else if(checkbox_Zigaretten.isSelected())
		{
			value = "Zigaretten";
		}
		
		else if(checkbox_Pfeife.isSelected())
		{
			value = "Pfeife";
		}
		else
		{
			value = ".";
		}
			
			
		return value;
	}
	
	
	
	
	
	public String getAlkFrequenz()
	{
		
		
		String value = null;
		
		if(radio_alk_nie.isSelected())
		{
			value = "nie";
		}
		else if(radio_alk_gelegentlich.isSelected())
		{
			value = "gelegentlich";
		}
		else if(radio_alk_t�glich.isSelected())
		{
			value = "t�glich";
		}
			
		return value;
	}
	
	
	
	public String getGesundheitlicheBedenken()
	{
		
		String value = null;
		
		if(radio_keine.isSelected())
		{
			value = "keine";
		}
		else if(radio_unterVoraussetzungen.isSelected())
		{
			value = "unter Voraussetzungen";
		}
		else if(radio_ja.isSelected())
		{
			value = "ja";
		}
			
		return value;
	}
	
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	
	
	
	
	
	
	//Felder bef�llen, wenn schon bearbeitet wurde...
	
	
	public String getAlkoholProTag()
	{
		return tf_AlkoholProTag.getText();
	}

	public void setTf_ID(String id) {
		
		tf_ID.setText(id);
		
	}

	
//-------------------------------

	public void setRadioRauchenNie()
	{
		radio_nie.setSelected(true);
	}

	public void setRadioRauchenEhemals()
	{
		radio_ehemals.setSelected(true);
	}
	
	
	public void setCBRauchPfeife()
	{
		checkbox_Pfeife.setSelected(true);
	}
	
	public void setCBRauchenZigarette()
	{
		checkbox_Zigaretten.setSelected(true);
	}
	
	
	public void setRadioAlkoholNie()
	{
		radio_alk_nie.setSelected(true);
	}
	
	public void setRadioAlkoholGelegentlich()
	{
		radio_alk_gelegentlich.setSelected(true);
	}
	
	public void setRadioAlkoholTaeglich()
	{
		radio_alk_t�glich.setSelected(true);
	}
	
	
	
	public void setRadioBedenkenKeine()
	{
	
		radio_keine.setSelected(true);
	}
	
	public void setRadioBedenkenJa()
	{
		
		radio_ja.setSelected(true);
	}
	
	public void setRadioBedenkenVoraussetzung()
	{
		radio_unterVoraussetzungen.setSelected(true);
	}
	

	
	
	
	
	
	//---------------------------------------------------
	
	
	
	
	
	public void setAlterStartRauchen(String alterStartRauchen) {
		tf_alterStartRauchen.setText(alterStartRauchen);
	}
	

	public void setJahreNichtraucher(String jahreNichtraucher) {
		tf_JahreNichtraucher.setText(jahreNichtraucher);
	}

	
	public void setAlkoholProTag(String alkoholProTag) {
		tf_AlkoholProTag.setText(alkoholProTag);
	}

	
	public void setZigarettenAnzahl(String zigarettenAnzahl) {
		tf_ZigarettenAnzahl.setText(zigarettenAnzahl);
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
