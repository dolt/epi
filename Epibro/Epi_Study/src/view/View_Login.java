package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;

public class View_Login extends JDialog {

	private final JPanel contentPanel = new JPanel();
	
	private TitledBorder title; //a border that surrounds the login field, with a headline
	private Font font = new Font("Arial", Font.BOLD, 20); //special pre-created font
	private JTextField tf_user;
	private JPasswordField passwordField;
	private JButton btnLogin;
	
	/**
	 * Create the dialog.
	 */
	public View_Login() {
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1044, 603);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("Herzinfarktverbund");
		label.setFont(new Font("Algerian", Font.PLAIN, 25));
		label.setBounds(556, 18, 290, 51);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("Baden-W\u00FCrttemberg");
		label_1.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_1.setBounds(741, 59, 290, 34);
		contentPanel.add(label_1);
		
		JLabel label_3 = new JLabel("Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		label_3.setFont(new Font("Verdana", Font.BOLD, 23));
		label_3.setBounds(233, 146, 533, 26);
		contentPanel.add(label_3);
		
		JLabel lblBorderlabel = new JLabel("");
		lblBorderlabel.setBounds(190, 256, 601, 221);
		contentPanel.add(lblBorderlabel);
		
		
		
		title = BorderFactory.createTitledBorder(""); //creates a new TitledBorder with the title "Login"
		title.setTitleJustification(TitledBorder.CENTER); //sets the title location to the middle top of the border
		title.setTitleFont(font); //set the font created at the top as the standard font (Arial, Bold, 20)
		lblBorderlabel.setBorder(title);
		
		btnLogin = new JButton("Login");
		btnLogin.setBounds(458, 413, 91, 23);
		contentPanel.add(btnLogin);
		
		tf_user = new JTextField();
		tf_user.setBounds(365, 318, 314, 20);
		contentPanel.add(tf_user);
		tf_user.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(365, 365, 314, 20);
		contentPanel.add(passwordField);
		
		JLabel lblUser = new JLabel("User:");
		lblUser.setFont(new Font("Verdana", Font.BOLD, 15));
		lblUser.setBounds(247, 319, 91, 14);
		contentPanel.add(lblUser);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Verdana", Font.BOLD, 15));
		lblPassword.setBounds(247, 366, 91, 14);
		contentPanel.add(lblPassword);
		
		JLabel lblNewLabel = new JLabel("Dateneingabe");
		lblNewLabel.setFont(new Font("Verdana", Font.BOLD, 15));
		lblNewLabel.setBounds(431, 183, 142, 26);
		contentPanel.add(lblNewLabel);
		
		
	
	}
	
	
	public void addLoginButtonActionListener(ActionListener al)
	{
		
		btnLogin.addActionListener(al);
		
	}
	
	public String getUsername()
	{
		return tf_user.getText();
	}
	
	public String getPassword()
	{
		return passwordField.getText();
		
	}
}
