
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import javax.swing.JRadioButton;

import model.Model;

public class View_FollowUP extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lbl_ID;
	public JLabel getLbl_ID() {
		return lbl_ID;
	}

	
	
	
	public void clearAll()
	{
		tf_ID.setText("");
		dateChooser_DatumFollowUP.setDate(null);
		dateChooserInfarkt.setDate(null);
		RBDiedGroup.clearSelection();
		RBInfraktGroup.clearSelection();
		
	}
	
	
	
	
	
	public void setTf_ID(String id)
	{
		tf_ID.setText(id);
	}
	
	public String getTf_ID()
	{
		return tf_ID.getText();
	}
	
	
	public void setLbl_ID(JLabel lbl_ID) {
		this.lbl_ID = lbl_ID;
	}

	public Boolean getRdbtn_Infarkt_Y() {
		return rdbtn_Infarkt_Y.isSelected();
	}

	public void setRdbtn_Infarkt_Y(JRadioButton rdbtn_Infarkt_Y) {
		this.rdbtn_Infarkt_Y = rdbtn_Infarkt_Y;
	}

	public Boolean getRdbtn_Infarkt_N() {
		return rdbtn_Infarkt_N.isSelected();
	}

	public void setRdbtn_Infarkt_N(JRadioButton rdbtn_Infarkt_N) {
		this.rdbtn_Infarkt_N = rdbtn_Infarkt_N;
	}

	public Boolean getRdbtn_Died_Y() {
		
		
		return rdbtn_Died_Y.isSelected();
		
		
	}

	public void setRdbtn_Died_Y(JRadioButton rdbtn_Died_Y) {
		
		this.rdbtn_Died_Y = rdbtn_Died_Y;
	}

	
	
	public Boolean getRdbtn_Died_N() {
		
		
		return rdbtn_Died_N.isSelected();
		
		
		
	}

	public void setRdbtn_Died_N(JRadioButton rdbtn_Died_N) {
		this.rdbtn_Died_N = rdbtn_Died_N;
	}

	public ButtonGroup getRBDiedGroup() {
		return RBDiedGroup;
	}

	public void setRBDiedGroup(ButtonGroup rBDiedGroup) {
		RBDiedGroup = rBDiedGroup;
	}

	public ButtonGroup getRBInfraktGroup() {
		return RBInfraktGroup;
	}

	public void setRBInfraktGroup(ButtonGroup rBInfraktGroup) {
		RBInfraktGroup = rBInfraktGroup;
	}


	
	public void setDatumInfarkt(String datum) 
	{
	
		DateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMAN);
		try 
		{
		java.util.Date date = format.parse(datum);
		dateChooserInfarkt.setDate(date);
		
		} 
		catch (ParseException e) 
		{
		e.printStackTrace();
		}
	
	}
	
	
	public void setDatumFollowUp(String datum) 
	{
	
		DateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMAN);
		try 
		{
		java.util.Date date = format.parse(datum);
		dateChooser_DatumFollowUP.setDate(date);
		
		} 
		catch (ParseException e) 
		{
		e.printStackTrace();
		}
	
	}
	
	
	
	public String getDatumInfarkt()
	{ 
		return ((JTextField)dateChooserInfarkt.getDateEditor().getUiComponent()).getText(); 
	}
	
	public String getDatumFollowUp()
	{ 
		return ((JTextField)dateChooser_DatumFollowUP.getDateEditor().getUiComponent()).getText(); 
	}
	
	
	
	
	public JPanel getContentPanel() {
		return contentPanel;
	}
	private JRadioButton rdbtn_Infarkt_Y;
	private JRadioButton rdbtn_Infarkt_N;
	private JRadioButton rdbtn_Died_Y;
	private JRadioButton rdbtn_Died_N;
	private JButton btn_Zurck;
	
	private ButtonGroup RBDiedGroup;
	private ButtonGroup RBInfraktGroup;
	private JDateChooser dateChooserInfarkt;
	private JDateChooser dateChooser_DatumFollowUP;
	private JButton btnVerlassen;

	private JTextField tf_ID;
	/**
	 * Create the dialog.
	 */
	public View_FollowUP(Model model) {
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1090, 960);
		
		//-----------------------------------------------------
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("ID:");
		label.setFont(new Font("Arial", Font.BOLD, 12));
		label.setBounds(44, 39, 23, 14);
		contentPanel.add(label);
		
		tf_ID = new JTextField(6);
		tf_ID.setEditable(false);
		tf_ID.setBounds(76, 37, 86, 20);
		contentPanel.add(tf_ID);
		
		JLabel label_2 = new JLabel("Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		label_2.setFont(new Font("Verdana", Font.BOLD, 23));
		label_2.setBounds(247, 152, 587, 26);
		contentPanel.add(label_2);
		
		JLabel lblFollowUP = new JLabel("FollowUP");
		lblFollowUP.setFont(new Font("Verdana", Font.BOLD, 15));
		lblFollowUP.setBounds(423, 189, 92, 26);
		contentPanel.add(lblFollowUP);
		
		JLabel lblDatum = new JLabel("Datum:");
		lblDatum.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDatum.setBounds(215, 255, 77, 14);
		contentPanel.add(lblDatum);
		
		JLabel label_1 = new JLabel("Herzinfarktverbund");
		label_1.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_1.setBounds(625, 18, 290, 51);
		contentPanel.add(label_1);
		
		JLabel label_4 = new JLabel("Baden-W\u00FCrttemberg");
		label_4.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_4.setBounds(741, 59, 290, 34);
		contentPanel.add(label_4);
		
		//-----------------------------------------------------
	

		JLabel lblInfarkt = new JLabel("Infarkt");
		lblInfarkt.setBounds(215, 347, 46, 14);
		contentPanel.add(lblInfarkt);

		JLabel lblVerstorben = new JLabel("Verstorben");
		lblVerstorben.setBounds(215, 452, 118, 14);
		contentPanel.add(lblVerstorben);

		JLabel lblInfarktDate = new JLabel("wenn ja : Infarktdatum");
		lblInfarktDate.setBounds(215, 384, 165, 39);
		contentPanel.add(lblInfarktDate);

		 dateChooserInfarkt = new JDateChooser();
		 dateChooserInfarkt.setBounds(423, 404, 87, 20);
		contentPanel.add(dateChooserInfarkt);

		dateChooser_DatumFollowUP = new JDateChooser();
		dateChooser_DatumFollowUP.setBounds(281, 255, 109, 20);
		contentPanel.add(dateChooser_DatumFollowUP);


		JLabel lblborder = new JLabel("");
		lblborder.setBorder(BorderFactory.createLineBorder(Color.black));
		lblborder.setBounds(207, 314, 183, 202);
		contentPanel.add(lblborder);

		JLabel labelborder = new JLabel("");
		labelborder.setBorder(BorderFactory.createLineBorder(Color.black));
		labelborder.setBounds(390, 314, 249, 202);
		contentPanel.add(labelborder);

		rdbtn_Infarkt_Y = new JRadioButton("Ja");
		rdbtn_Infarkt_Y.setBounds(401, 343, 109, 23);
		contentPanel.add(rdbtn_Infarkt_Y);

		rdbtn_Infarkt_N = new JRadioButton("Nein");
		rdbtn_Infarkt_N.setBounds(549, 343, 63, 23);
		contentPanel.add(rdbtn_Infarkt_N);

		rdbtn_Died_Y = new JRadioButton("Ja");
		rdbtn_Died_Y.setBounds(401, 448, 109, 23);
		contentPanel.add(rdbtn_Died_Y);

		rdbtn_Died_N = new JRadioButton("Nein");
		rdbtn_Died_N.setBounds(549, 448, 63, 23);
		contentPanel.add(rdbtn_Died_N);
		
		btnVerlassen = new JButton("Verlassen+Speichern");
		btnVerlassen.setBounds(780, 858, 135, 23);
		contentPanel.add(btnVerlassen);

		ButtonGroup RBInfraktGroup = new ButtonGroup();
		RBInfraktGroup.add(rdbtn_Infarkt_Y);
		RBInfraktGroup.add(rdbtn_Infarkt_N);

		ButtonGroup RBDiedGroup = new ButtonGroup();
		RBDiedGroup.add(rdbtn_Died_Y);
		RBDiedGroup.add(rdbtn_Died_N);

	
	
	
		btn_Zurck = new JButton("zur\u00FCck");
		btn_Zurck.setBounds(661, 858, 91, 23);
		contentPanel.add(btn_Zurck);
	
	
	}

	public void hideInfaktDate() {
		dateChooserInfarkt.setVisible(false);
	}

	

	public void addDiedYet_YButtonListener(ActionListener a) {
		rdbtn_Died_Y.addActionListener(a);
	}

	public void addDiedYet_NButtonListener(ActionListener a) {
		rdbtn_Died_N.addActionListener(a);
	}

	public void addrdbtn_Infarkt_YButtonListener(ActionListener a) {
		rdbtn_Infarkt_Y.addActionListener(a);
	}

	public void addrdbtn_Infarkt_NButtonListener(ActionListener a) {
		rdbtn_Infarkt_N.addActionListener(a);
	}


	public void addBackButtonListener(ActionListener a) {

		btn_Zurck.addActionListener(a);

	}

	public void showInfaktDate() {
		// TODO Auto-generated method stub
		dateChooserInfarkt.setVisible(true);
	}
}
