package view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import javax.swing.JRadioButton;

import model.Model;

public class View_BodyCheck extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldsystol;
	private JTextField textField_dia;
	private JTextField textField_puls;
	private JButton btn_Zurck;
	private JButton btn_Weiter;
	private JTextField tf_ID;
	private JRadioButton rdbtnDieag_N;
	private JRadioButton rdbtnDieag_Y;
	private JDateChooser dateChooser;
	ButtonGroup RBGroup ;

	/**
	 * Create the dialog.
	 */
	public View_BodyCheck(Model model) {

		// --------------------------------------------------------------------------------
		setResizable(false);
		setBounds(100, 100, 1090, 960);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel label = new JLabel("ID:");
		label.setFont(new Font("Arial", Font.BOLD, 12));
		label.setBounds(44, 39, 23, 14);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("Herzinfarktverbund");
		label_1.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_1.setBounds(625, 18, 290, 51);
		contentPanel.add(label_1);
		
		JLabel label_4 = new JLabel("Baden-W\u00FCrttemberg");
		label_4.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_4.setBounds(741, 59, 290, 34);
		contentPanel.add(label_4);
		tf_ID = new JTextField(6);
		tf_ID.setEditable(false);
		tf_ID.setBounds(76, 37, 86, 20);
		contentPanel.add(tf_ID);

		JLabel label_2 = new JLabel(
				"Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		label_2.setFont(new Font("Verdana", Font.BOLD, 23));
		label_2.setBounds(247, 152, 587, 26);
		contentPanel.add(label_2);

		JLabel lblCHDPara = new JLabel(
				"CHD-Parameter zum Zeitpunkt der körperlichen Untersuchung");
		lblCHDPara.setFont(new Font("Verdana", Font.BOLD, 15));
		lblCHDPara.setBounds(331, 189, 544, 26);
		contentPanel.add(lblCHDPara);

		JLabel lblDatum = new JLabel("Datum:");
		lblDatum.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDatum.setBounds(215, 255, 77, 14);
		contentPanel.add(lblDatum);
		// --------------------------------------------------------------------------
		{
			JLabel lblDieganostizierteHypertonie = new JLabel(
					"Dieganostizierte Hypertonie ");
			lblDieganostizierteHypertonie.setBounds(397, 337, 161, 14);
			contentPanel.add(lblDieganostizierteHypertonie);
		}
		{
			JLabel lblSystolischerBlutdruck = new JLabel(
					"systolischer Blutdruck");
			lblSystolischerBlutdruck.setBounds(397, 397, 131, 23);
			contentPanel.add(lblSystolischerBlutdruck);
		}
		{
			JLabel lblDiastolischerBlutdruck = new JLabel(
					"diastolischer Blutdruck");
			lblDiastolischerBlutdruck.setBounds(397, 457, 117, 23);
			contentPanel.add(lblDiastolischerBlutdruck);
		}
		{
			JLabel lblPuls = new JLabel("Puls");
			lblPuls.setBounds(397, 520, 46, 14);
			contentPanel.add(lblPuls);
		}

		textFieldsystol = new JTextField();
		textFieldsystol.setBounds(609, 398, 86, 20);
		contentPanel.add(textFieldsystol);
		textFieldsystol.setColumns(10);

		textField_dia = new JTextField();
		textField_dia.setBounds(609, 458, 86, 20);
		contentPanel.add(textField_dia);
		textField_dia.setColumns(10);

		textField_puls = new JTextField();
		textField_puls.setBounds(609, 517, 86, 20);
		contentPanel.add(textField_puls);
		textField_puls.setColumns(10);

		dateChooser = new JDateChooser();
		dateChooser.setBounds(323, 255, 87, 20);
		contentPanel.add(dateChooser);

		JLabel lblMmgh = new JLabel("mmHg");
		lblMmgh.setBounds(756, 393, 78, 30);
		contentPanel.add(lblMmgh);

		JLabel lblMmhg = new JLabel("mmHg");
		lblMmhg.setBounds(756, 461, 46, 14);
		contentPanel.add(lblMmhg);

		JLabel lblSchlgeminute = new JLabel("Schl\u00E4ge/Minute");
		lblSchlgeminute.setBounds(756, 520, 97, 14);
		contentPanel.add(lblSchlgeminute);

		JLabel lblborder = new JLabel("");
		lblborder.setBorder(BorderFactory.createLineBorder(Color.black));
		lblborder.setBounds(352, 311, 231, 252);
		contentPanel.add(lblborder);

		JLabel labelborder2 = new JLabel("");
		labelborder2.setBorder(BorderFactory.createLineBorder(Color.black));
		labelborder2.setBounds(583, 311, 278, 252);
		contentPanel.add(labelborder2);

		rdbtnDieag_Y = new JRadioButton("Ja");
		rdbtnDieag_Y.setBounds(609, 333, 63, 23);
		contentPanel.add(rdbtnDieag_Y);

		rdbtnDieag_N = new JRadioButton("Nein");
		rdbtnDieag_N.setBounds(736, 333, 71, 23);
		contentPanel.add(rdbtnDieag_N);
		RBGroup = new ButtonGroup();
		RBGroup.add(rdbtnDieag_Y);
		RBGroup.add(rdbtnDieag_N);

		btn_Weiter = new JButton("weiter");
		btn_Weiter.setBounds(977, 888, 97, 23);
		contentPanel.add(btn_Weiter);

		btn_Zurck = new JButton("zur\u00FCck");
		btn_Zurck.setBounds(812, 888, 108, 23);
		contentPanel.add(btn_Zurck);

	}

	public String getTextFieldsystol() {
		return textFieldsystol.getText();
	}

	public void setTextFieldsystol(String textFieldsystol) {
		this.textFieldsystol.setText(textFieldsystol);
	}

	public String getTextField_dia() {
		return textField_dia.getText();
	}

	public void setTextFieldsdia(String textFieldsystol) {
		this.textField_dia.setText(textFieldsystol);
	}

	public String getTextField_puls() {
		return textField_puls.getText();
	}

	public void setTextField_puls(String textField_puls) {
		this.textField_puls.setText(textField_puls);
	}

	public boolean getRdbtnDieag_N() {
		return rdbtnDieag_N.isSelected();
	}

	public void setRdbtnDieag_N(boolean rdbtnDieag_N) {
		this.rdbtnDieag_N.setEnabled(rdbtnDieag_N);
	}

	public boolean getRdbtnDieag_Y() {
		return rdbtnDieag_Y.isSelected();
	}

	public void setRdbtnDieag_Y(boolean rdbtnDieag_Y) {
		this.rdbtnDieag_Y.setEnabled(rdbtnDieag_Y);
	}

	public void addFwardButtonListener(ActionListener a) {

		btn_Weiter.addActionListener(a);

	}

	public void addBackButtonListener(ActionListener a) {

		btn_Zurck.addActionListener(a);

	}
	public String getTf_ID(){
		return tf_ID.getText();
	}
	public void setTf_ID(String id){
		tf_ID.setText(id);
	}
	public void clearAll(){
		setTextField_puls("");
		setTextFieldsdia("");
		setTextFieldsystol("");
		RBGroup.clearSelection();
		dateChooser.setDate(null);
		
		
		
		
	}

	
	public String getDateChooser() {
		return ((JTextField) dateChooser.getDateEditor().getUiComponent()).getText();

	}

	/**
	 * @param dateChooser
	 *            the dateChooser to set
	 */
	public void setDateChooser(String Datum) {

		DateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMAN);
		try {
			Date date = format.parse(Datum);
			dateChooser.setDate(date);

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
