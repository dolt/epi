package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JComboBox;

public class View_Menu extends JDialog {

	private final JPanel contentPanel = new JPanel();
	
	private TitledBorder title; //a border that surrounds the login field, with a headline
	private Font font = new Font("Arial", Font.BOLD, 20); //special pre-created font

	
	private JButton btnStammdatenAnlegen;
	private JButton btnVerlassen;
	
	private JTextField tf_umWasGehts;
	
	private JButton btnBearbeiten;
	private JTextField tf_FallID;
	
	
	private JComboBox ladeFall;
	private JTextField tf_user;
	
	
	
	/**
	 * Create the dialog.
	 */
	public View_Menu() {
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 1079, 697);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("Herzinfarktverbund");
		label.setFont(new Font("Algerian", Font.PLAIN, 25));
		label.setBounds(556, 18, 290, 51);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("Baden-W\u00FCrttemberg");
		label_1.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_1.setBounds(741, 59, 290, 34);
		contentPanel.add(label_1);
		
		JLabel label_3 = new JLabel("Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		label_3.setFont(new Font("Verdana", Font.BOLD, 23));
		label_3.setBounds(247, 152, 587, 26);
		contentPanel.add(label_3);
		
		JLabel lblBorderlabel = new JLabel("");
		lblBorderlabel.setBounds(203, 290, 601, 221);
		contentPanel.add(lblBorderlabel);
		
		btnStammdatenAnlegen = new JButton("Neuen Fall anlegen");
		btnStammdatenAnlegen.setBounds(247, 344, 169, 23);
		contentPanel.add(btnStammdatenAnlegen);
		
		btnBearbeiten = new JButton("Fall bearbeiten");
		btnBearbeiten.setBounds(553, 387, 169, 23);
		contentPanel.add(btnBearbeiten);
		
		
		
		title = BorderFactory.createTitledBorder("MENU"); //creates a new TitledBorder with the title "Login"
		title.setTitleJustification(TitledBorder.CENTER); //sets the title location to the middle top of the border
		title.setTitleFont(font); //set the font created at the top as the standard font (Arial, Bold, 20)
		lblBorderlabel.setBorder(title);
		
		ladeFall = new JComboBox();
		ladeFall.setBounds(553, 344, 169, 22);
		ladeFall.addItem("Fall ausw�hlen");
		contentPanel.add(ladeFall);
		
		btnVerlassen = new JButton("Verlassen");
		btnVerlassen.setBounds(247, 387, 169, 23);
		contentPanel.add(btnVerlassen);
		
		JButton btnbersicht = new JButton("\u00DCbersicht");
		btnbersicht.setBounds(553, 431, 169, 23);
		contentPanel.add(btnbersicht);
		
		tf_umWasGehts = new JTextField();
		tf_umWasGehts.setBounds(430, 549, 86, 20);
		contentPanel.add(tf_umWasGehts);
		tf_umWasGehts.setColumns(10);
		tf_umWasGehts.setVisible(false);
		
		tf_FallID = new JTextField();
		tf_FallID.setBounds(430, 580, 86, 20);
		contentPanel.add(tf_FallID);
		tf_FallID.setColumns(10);
		tf_FallID.setVisible(false);
		
		tf_user = new JTextField();
		tf_user.setBounds(430, 611, 86, 20);
		contentPanel.add(tf_user);
		tf_user.setColumns(10);
		tf_user.setVisible(false);
		
		
	
	}
	
	
	public void addVerlassenButtonActionListener(ActionListener al)
	{
		
		btnVerlassen.addActionListener(al);
		
	}
	
	public void addStammdatenAnlegenButtonListener(ActionListener al)
	{
		btnStammdatenAnlegen.addActionListener(al);
	}
	
	
	public void addBearbeitenButtonListener(ActionListener al)
	{
		btnBearbeiten.addActionListener(al);
	}
	
	
	public void setUserID(String id)
	{
		tf_user.setText(id);
	}
	
	public String getUserID()
	{
		return tf_user.getText();
	}
	
	
	public void setFallID(String id)
	{
		tf_FallID.setText(id);
	}
	
	public String getFallID()
	{
		return tf_FallID.getText();
	}
	
	
	public void setUmWasgehts(String number)
	{
		tf_umWasGehts.setText(number); 
	}
	
	public String getUmwasgehts()
	{
		return tf_umWasGehts.getText();
	}
	
	
	
	
	
	public void fillComboBoxProject(List list) //comboBox ladeFall...
	{
		
	
		if(ladeFall.getItemCount() <= 1) //just fill the combo box if there are less than 1 entrys, because otherwise it will always list the same project many times
		{
		
			for(int i = 0; i < list.getItemCount(); i++ )
			{
				ladeFall.addItem(list.getItem(i));
			}
		}
		
	}
	
	
	public String getSelectedCase()
	{
		
		String item = (String) ladeFall.getSelectedItem();
		
		return item;
		
	
	}
	
	
}
