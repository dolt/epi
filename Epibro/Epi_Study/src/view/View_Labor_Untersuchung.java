package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import model.Model;

import javax.swing.JLabel;

import java.awt.Font;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JCalendar;

public class View_Labor_Untersuchung extends JDialog {

	private final JPanel contentPanel = new JPanel();

	
	private Model model;

	
	private JButton button_LaborZurück;
	private JButton button_LaborWeiter;
	
	
	
	private JLabel label;
	private JTextField tf_ID;
	private JLabel label_1;
	private JLabel lblHerzinfarktverbund;
	private JLabel lblLaborwerteZumZeitpunkt;
	private JLabel label_3;
	private JLabel lblCholesterin;
	private JLabel lblCreatinin;
	private JLabel lblTriglyceride;
	private JLabel lblBlutsenkungNach;
	private JLabel lblBlutsenkungNach_1;
	private JTextField tf_Cholesterin;
	private JTextField tf_Creatinin;
	private JTextField tf_Triglyz;
	private JTextField tf_BSG1;
	private JTextField tf_BSG2;
	private JLabel lblMmol;
	private JLabel label_4;
	private JLabel lblMm;
	private JLabel label_5;
	private JLabel lblmoll;
	private JLabel lblBadenwrttemberg;
	private JLabel label_li;
	private JLabel label_re;

	
	private LineBorder border;
	
	private JDateChooser dateChooser_DatumAufnahmeLabor;
	private JButton button;
	
	
	/**
	 * Create the dialog.
	 */
	public View_Labor_Untersuchung(Model model) {
		
		
		this.model	= model;
		
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		
		setResizable(false);
		setBounds(100, 100, 1090, 960);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		button_LaborWeiter = new JButton("weiter");
		button_LaborWeiter.setBounds(940, 858, 91, 23);
		contentPanel.add(button_LaborWeiter);
		
		button_LaborZurück = new JButton("zur\u00FCck");
		button_LaborZurück.setBounds(661, 858, 91, 23);
		contentPanel.add(button_LaborZurück);
		
		label = new JLabel("ID:");
		label.setFont(new Font("Arial", Font.BOLD, 12));
		label.setBounds(44, 39, 23, 14);
		contentPanel.add(label);
		
		tf_ID = new JTextField(6);
		tf_ID.setEditable(false);
		tf_ID.setBounds(76, 37, 86, 20);
		contentPanel.add(tf_ID);
		
		label_1 = new JLabel("Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		label_1.setFont(new Font("Verdana", Font.BOLD, 23));
		label_1.setBounds(247, 152, 587, 26);
		contentPanel.add(label_1);
		
		lblHerzinfarktverbund = new JLabel("Herzinfarktverbund");
		lblHerzinfarktverbund.setFont(new Font("Algerian", Font.PLAIN, 25));
		lblHerzinfarktverbund.setBounds(625, 18, 290, 51);
		contentPanel.add(lblHerzinfarktverbund);
		
		lblLaborwerteZumZeitpunkt = new JLabel("Laborwerte zum Zeitpunkt der k\u00F6rperlichen Untersuchung");
		lblLaborwerteZumZeitpunkt.setFont(new Font("Verdana", Font.BOLD, 15));
		lblLaborwerteZumZeitpunkt.setBounds(257, 189, 492, 26);
		contentPanel.add(lblLaborwerteZumZeitpunkt);
		
		label_3 = new JLabel("Datum:");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_3.setBounds(215, 255, 77, 14);
		contentPanel.add(label_3);
		
		lblCholesterin = new JLabel("Cholesterin");
		lblCholesterin.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblCholesterin.setBounds(306, 336, 150, 14);
		contentPanel.add(lblCholesterin);
		
		lblCreatinin = new JLabel("Creatinin");
		lblCreatinin.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblCreatinin.setBounds(306, 402, 77, 14);
		contentPanel.add(lblCreatinin);
		
		lblTriglyceride = new JLabel("Triglyceride");
		lblTriglyceride.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblTriglyceride.setBounds(306, 467, 77, 14);
		contentPanel.add(lblTriglyceride);
		
		lblBlutsenkungNach = new JLabel("Blutsenkung nach 1 Stunde");
		lblBlutsenkungNach.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblBlutsenkungNach.setBounds(306, 533, 150, 14);
		contentPanel.add(lblBlutsenkungNach);
		
		lblBlutsenkungNach_1 = new JLabel("Blutsenkung nach 2 Stunden");
		lblBlutsenkungNach_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblBlutsenkungNach_1.setBounds(306, 597, 150, 14);
		contentPanel.add(lblBlutsenkungNach_1);
		
		tf_Cholesterin = new JTextField();
		tf_Cholesterin.setBounds(499, 333, 110, 20);
		contentPanel.add(tf_Cholesterin);
		tf_Cholesterin.setColumns(10);
		
		tf_Creatinin = new JTextField();
		tf_Creatinin.setColumns(10);
		tf_Creatinin.setBounds(499, 399, 110, 20);
		contentPanel.add(tf_Creatinin);
		
		tf_Triglyz = new JTextField();
		tf_Triglyz.setColumns(10);
		tf_Triglyz.setBounds(499, 464, 110, 20);
		contentPanel.add(tf_Triglyz);
		
		tf_BSG1 = new JTextField();
		tf_BSG1.setColumns(10);
		tf_BSG1.setBounds(499, 530, 110, 20);
		contentPanel.add(tf_BSG1);
		
		tf_BSG2 = new JTextField();
		tf_BSG2.setColumns(10);
		tf_BSG2.setBounds(499, 594, 110, 20);
		contentPanel.add(tf_BSG2);
		
		lblMmol = new JLabel("mmol/l");
		lblMmol.setBounds(630, 336, 46, 14);
		contentPanel.add(lblMmol);
		
		label_4 = new JLabel("mmol/l");
		label_4.setBounds(630, 467, 46, 14);
		contentPanel.add(label_4);
		
		lblMm = new JLabel("mm");
		lblMm.setBounds(630, 533, 46, 14);
		contentPanel.add(lblMm);
		
		label_5 = new JLabel("mm");
		label_5.setBounds(630, 597, 46, 14);
		contentPanel.add(label_5);
		
		lblmoll = new JLabel("\u00B5mol/l");
		lblmoll.setBounds(630, 402, 46, 14);
		contentPanel.add(lblmoll);
		
		lblBadenwrttemberg = new JLabel("Baden-W\u00FCrttemberg");
		lblBadenwrttemberg.setFont(new Font("Algerian", Font.PLAIN, 25));
		lblBadenwrttemberg.setBounds(741, 59, 290, 34);
		contentPanel.add(lblBadenwrttemberg);
		
		
		border = (LineBorder) BorderFactory.createLineBorder(Color.black);
		
		label_li = new JLabel("");
		label_li.setBorder(border);
		label_li.setBounds(259, 311, 226, 348);
		contentPanel.add(label_li);
		
		label_re = new JLabel("");
		label_re.setBorder(border);
		label_re.setBounds(483, 311, 256, 348);
		contentPanel.add(label_re);
		
		
		
		dateChooser_DatumAufnahmeLabor = new JDateChooser();
		dateChooser_DatumAufnahmeLabor.setBounds(281, 255, 118, 20);
		contentPanel.add(dateChooser_DatumAufnahmeLabor);
		
		button = new JButton("Verlassen+Speichern");
		button.setBounds(780, 858, 135, 23);
		contentPanel.add(button);
		
	}
	
	
	public void clearAll()
	{
		
		dateChooser_DatumAufnahmeLabor.setDate(null);
		tf_ID.setText("");
		tf_Cholesterin.setText("");
		tf_Creatinin.setText("");
		tf_Triglyz.setText("");
		tf_BSG1.setText("");
		tf_BSG2.setText("");
		
		
	}
	
	
	
	public void addLaborWeiterButtonListener(ActionListener al)
	{
		button_LaborWeiter.addActionListener(al);
	}

	
	public void addLaborZurückButtonListener(ActionListener al)
	{
		button_LaborZurück.addActionListener(al);
	}
	
	
	//Werte aus Feldern...
	
	
	public String getID()
	{
		return tf_ID.getText();
	}
	
	
	public String getDatumLaborAufnahme()
	{ 
		return ((JTextField)dateChooser_DatumAufnahmeLabor.getDateEditor().getUiComponent()).getText(); 
	}
	
	
	public String getCholesterin()
	{
		return tf_Cholesterin.getText();
	}
	
	public String getCreatinin()
	{
		return tf_Creatinin.getText();
	}
	
	public String getTriglyceride()
	{
		return tf_Triglyz.getText();
	}
	
	public String getBSG1()
	{
		return tf_BSG1.getText();
	}
	
	public String getBSG2()
	{
		return tf_BSG2.getText();
	}
	
	
	
	//Felder befüllen (wenn View schon bearbeitet wurde)
	

public void setTf_ID(String id) {
		
		tf_ID.setText(id);
		
	}

	public void setCholesterin(String cholesterin) {

		tf_Cholesterin.setText(cholesterin);
	}

	public void setCreatinin(String creatinin) {
		tf_Creatinin.setText(creatinin);
	}

	public void setTriglyz(String triglyz) {
		tf_Triglyz.setText(triglyz);
	}

	public void setBSG1(String bsg1) {
		tf_BSG1.setText(bsg1);
	}

	public void setBSG2(String bsg2) {
		tf_BSG2.setText(bsg2);
	}
	
	
	
	public void setDatum(String datum) 
	{
		
		DateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMAN);
		try 
		{
			java.util.Date date = format.parse(datum);
			dateChooser_DatumAufnahmeLabor.setDate(date);
			
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		
	}

	
	
	
	
	
	
	
	
}

