package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import model.Model;

import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class View_Stammdaten extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField tf_ID;
	private JTextField tf_zentrum;
	private JTextField tf_gewicht;
	private JTextField tf_groesse;

	private JButton btnWeiter;
	private JButton btnMen�;
	
	
	private LineBorder border;
	
	private JDateChooser dateChooser_DatumAuf;
	private JDateChooser dateChooser_Geburtsdatum;
	
	
	private Model model;

	/**
	 * Create the dialog.
	 */
	public View_Stammdaten(Model model) {
		
		
		this.model = model;
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		
		setResizable(false);
		setBounds(100, 100, 1090, 960);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblInfuMort = new JLabel("Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		lblInfuMort.setFont(new Font("Verdana", Font.BOLD, 23));
		lblInfuMort.setBounds(247, 152, 587, 26);
		contentPanel.add(lblInfuMort);
		
		JLabel lblStammdaten = new JLabel("Stammdaten");
		lblStammdaten.setFont(new Font("Verdana", Font.BOLD, 15));
		lblStammdaten.setBounds(421, 194, 134, 26);
		contentPanel.add(lblStammdaten);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Arial", Font.BOLD, 12));
		lblId.setBounds(44, 39, 23, 14);
		contentPanel.add(lblId);
		
		JLabel lblAufnahmedatum = new JLabel("Aufnahmedatum:");
		lblAufnahmedatum.setFont(new Font("Arial", Font.PLAIN, 12));
		lblAufnahmedatum.setBounds(340, 338, 97, 14);
		contentPanel.add(lblAufnahmedatum);
		
		JLabel lblStudienzentrum = new JLabel("Studienzentrum:");
		lblStudienzentrum.setFont(new Font("Arial", Font.PLAIN, 12));
		lblStudienzentrum.setBounds(340, 401, 97, 14);
		contentPanel.add(lblStudienzentrum);
		
		JLabel lblGeburtsdatum = new JLabel("Geburtsdatum:");
		lblGeburtsdatum.setFont(new Font("Arial", Font.PLAIN, 12));
		lblGeburtsdatum.setBounds(353, 469, 84, 14);
		contentPanel.add(lblGeburtsdatum);
		
		JLabel lblGre = new JLabel("Gr\u00F6\u00DFe:");
		lblGre.setFont(new Font("Arial", Font.PLAIN, 12));
		lblGre.setBounds(399, 542, 38, 14);
		contentPanel.add(lblGre);
		
		JLabel lblGewicht = new JLabel("Gewicht:");
		lblGewicht.setFont(new Font("Arial", Font.PLAIN, 12));
		lblGewicht.setBounds(390, 616, 47, 14);
		contentPanel.add(lblGewicht);
		
		JLabel lblCm = new JLabel("cm");
		lblCm.setFont(new Font("Arial", Font.PLAIN, 12));
		lblCm.setBounds(668, 542, 23, 14);
		contentPanel.add(lblCm);
		
		JLabel lblKg = new JLabel("kg");
		lblKg.setFont(new Font("Arial", Font.PLAIN, 12));
		lblKg.setBounds(668, 616, 23, 14);
		contentPanel.add(lblKg);
		
		tf_ID = new JTextField(6);
		tf_ID.setEditable(false);
		tf_ID.setBounds(76, 37, 86, 20);
		contentPanel.add(tf_ID);
		tf_ID.setColumns(10);
		
		tf_zentrum = new JTextField();
		tf_zentrum.setBounds(536, 399, 110, 20);
		contentPanel.add(tf_zentrum);
		tf_zentrum.setColumns(10);
		
		tf_gewicht = new JTextField();
		tf_gewicht.setBounds(536, 614, 116, 20);
		contentPanel.add(tf_gewicht);
		tf_gewicht.setColumns(10);
		
		tf_groesse = new JTextField();
		tf_groesse.setBounds(536, 540, 112, 20);
		contentPanel.add(tf_groesse);
		tf_groesse.setColumns(10);
		
		btnWeiter = new JButton("weiter");
		btnWeiter.setBounds(940, 858, 91, 23);
		contentPanel.add(btnWeiter);
		
		JLabel label = new JLabel("Herzinfarktverbund");
		label.setFont(new Font("Algerian", Font.PLAIN, 25));
		label.setBounds(625, 18, 290, 51);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("Baden-W\u00FCrttemberg");
		label_1.setFont(new Font("Algerian", Font.PLAIN, 25));
		label_1.setBounds(741, 59, 290, 34);
		contentPanel.add(label_1);
		
		JLabel lineBorder_li = new JLabel("");
		lineBorder_li.setBounds(247, 312, 258, 366);
		contentPanel.add(lineBorder_li);
	
		
		border = (LineBorder) BorderFactory.createLineBorder(Color.black);
		lineBorder_li.setBorder(border);
		
		JLabel lineBorder_re = new JLabel("");
		lineBorder_re.setBounds(504, 312, 224, 366);
		contentPanel.add(lineBorder_re);
		lineBorder_re.setBorder(border);
		
		dateChooser_DatumAuf = new JDateChooser();
		dateChooser_DatumAuf.setBounds(536, 338, 110, 20);
		contentPanel.add(dateChooser_DatumAuf);
		
		dateChooser_Geburtsdatum = new JDateChooser();
		dateChooser_Geburtsdatum.setBounds(536, 469, 110, 20);
		contentPanel.add(dateChooser_Geburtsdatum);
		
		btnMen� = new JButton("Men\u00FC");
		btnMen�.setBounds(839, 858, 91, 23);
		contentPanel.add(btnMen�);
		
		
		
		
	}
	
	
	
	
	public void addWeiterButtonListener(ActionListener al)
	{
		btnWeiter.addActionListener(al);
	}
	
	
	public void addMen�ButtonListener(ActionListener al)
	{
		btnMen�.addActionListener(al);
	}
	
	
	
	
	public void clearAll()
	{
		tf_ID.setText("");
		tf_ID.setText("");
		dateChooser_DatumAuf.setDate(null);;
		dateChooser_Geburtsdatum.setDate(null);
		tf_zentrum.setText("");
		tf_gewicht.setText("");
		tf_groesse.setText("");
		
		
	}
	
	
	
	
	//Werte aus Feldern ziehen
	
	
	public String getTF_Id()
	{
		return tf_ID.getText();
	}
	
	
	public String getAufnahmeDatum()
	{ 
		return ((JTextField)dateChooser_DatumAuf.getDateEditor().getUiComponent()).getText(); 
	}
	
	public String getGeburtsDatum()
	{ 
		return ((JTextField)dateChooser_Geburtsdatum.getDateEditor().getUiComponent()).getText(); 
	}
	
	
	public String getZentrum()
	{
		return tf_zentrum.getText();
		
	}
	
	public String getGewicht()
	{
		return tf_gewicht.getText();
	}
	
	
	public String getGroesse()
	{
		return tf_groesse.getText();
	}



	
	//Felder bef�llen (wenn View schon bearbeitet wurde....)
	

	public void setTf_ID(String id) {
		
		tf_ID.setText(id);
		
	}




	public void setZentrum(String zentrum) {

		tf_zentrum.setText(zentrum);
		
	}




	public void setGewicht(String gewicht) {
		
		tf_gewicht.setText(gewicht);
	}




	public void setGroesse(String groesse) {
		
		tf_groesse.setText(groesse);
	}




	public void setDatumAufnahme(String datum) 
	{
		
		
		DateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMAN);
		try {
			Date date = format.parse(datum);
			dateChooser_DatumAuf.setDate(date);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}




	public void setGeburtsdatum(String geburtsdatum) {
		
		
		DateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMAN);
		try {
			Date date = format.parse(geburtsdatum);
			dateChooser_Geburtsdatum.setDate(date);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	
	}
	
	
	
	
	//Werte in Felder schreiben
	
	
	
	
	
}
