public class OwnClass_BodyCheck {

	private String Date;
	private String HypertonieYN;
	private String syssto;
	private String diasto;
	private String puls;
	private String ID;
	
	
	
	
	
	public OwnClass_BodyCheck(String ID,String Date, String Hypertonie, String sys, String dia,String puls){
		setDate(Date);
		setHypertonieYN(Hypertonie);
		setSyssto(sys);
		setDiasto(dia);
		setPuls(puls);
		setID(ID);
		
		
		
	}
	
	
	
	/**
	 * @return the hypertonieYN
	 */
	public String getHypertonieYN() {
		return HypertonieYN;
	}
	/**
	 * @param hypertonieYN the hypertonieYN to set
	 */
	public void setHypertonieYN(String hypertonieYN) {
		HypertonieYN = hypertonieYN;
	}
	/**
	 * @return the syssto
	 */
	public String getSyssto() {
		return syssto;
	}
	/**
	 * @param syssto the syssto to set
	 */
	public void setSyssto(String syssto) {
		this.syssto = syssto;
	}
	/**
	 * @return the diasto
	 */
	public String getDiasto() {
		return diasto;
	}
	/**
	 * @param diasto the diasto to set
	 */
	public void setDiasto(String diasto) {
		this.diasto = diasto;
	}
	/**
	 * @return the puls
	 */
	public String getPuls() {
		return puls;
	}
	/**
	 * @param puls the puls to set
	 */
	public void setPuls(String puls) {
		this.puls = puls;
	}



	/**
	 * @return the date
	 */
	public String getDate() {
		return Date;
	}



	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		Date = date;
	}



	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}



	/**
	 * @param iD the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

}
