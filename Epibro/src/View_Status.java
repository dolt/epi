import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


public class View_Status extends JDialog {

	private final JPanel contentPanel = new JPanel();
	//----------------------------------------------------------
	public void setTxtf_followUP(String txtf_followUP) {
		this.txtf_followUP.setText(txtf_followUP);
	}


	public void setTxtf_CHD(String txtf_CHD) {
		this.txtf_CHD.setText(txtf_CHD);
	}


	public void setTxtf_laborBody(String txtf_laborBody) {
		this.txtf_laborBody.setText(txtf_laborBody);
	}


	public void setTxtf_LaborFirstVisit(String txtf_LaborFirstVisit) {
		this.txtf_LaborFirstVisit.setText(txtf_LaborFirstVisit);
	}


	public void setTxtf_Expodata(String txtf_Expodata) {
		this.txtf_Expodata.setText(txtf_Expodata);
	}


	public void setTxtf_Stammdata(String txtf_Stammdata) {
		this.txtf_Stammdata.setText(txtf_Stammdata);
	}
	

	public void setTxtDate(String date) {
		// TODO Auto-generated method stub
		this.txtDate.setText(date);
	}	
	
//-------------------------------------------------------------------------------
private JLabel lblDatum;
	private JTextField txtf_followUP;
	private JTextField txtf_CHD;
	private JTextField txtf_laborBody;
	private JTextField txtf_LaborFirstVisit;
	private JTextField txtf_Expodata;
	private JTextField txtf_Stammdata;
	private JTextField txtDate;
	

	/**
	 * Launch the application.
	 */

	/**
	 * Create the dialog.
	 */
	public View_Status() {
		setResizable(false);
		setBounds(100, 100, 1090, 960);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);


		JLabel label_2 = new JLabel(
				"Infarkt und Mortalit\u00E4t bei Bauarbeitern");
		label_2.setBounds(247, 152, 587, 26);
		label_2.setFont(new Font("Verdana", Font.BOLD, 23));
		contentPanel.add(label_2);

		JLabel lblCHDPara = new JLabel(
				"Zwischenbericht/Auswertung");
		lblCHDPara.setBounds(331, 189, 544, 26);
		lblCHDPara.setFont(new Font("Verdana", Font.BOLD, 15));
		contentPanel.add(lblCHDPara);

		lblDatum= new JLabel("Datum:");
		lblDatum.setBounds(215, 255, 77, 14);
		lblDatum.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(lblDatum);
		JLabel label_1 = new JLabel("Herzinfarktverbund");
		label_1.setBounds(625, 18, 290, 51);
		label_1.setFont(new Font("Algerian", Font.PLAIN, 25));
		contentPanel.add(label_1);
		
		JLabel label_4 = new JLabel("Baden-W\u00FCrttemberg");
		label_4.setBounds(741, 59, 290, 34);
		label_4.setFont(new Font("Algerian", Font.PLAIN, 25));
		contentPanel.add(label_4);
		
		JLabel lblExpositionserhebung = new JLabel("Expositionserhebung");
		lblExpositionserhebung.setBounds(213, 395, 225, 14);
		contentPanel.add(lblExpositionserhebung);
		
		JLabel lblStammdaten = new JLabel("Stammdaten");
		lblStammdaten.setBounds(216, 342, 183, 14);
		contentPanel.add(lblStammdaten);
		
		JLabel lbl_FirstVisit = new JLabel("Laborwerte zum Zeitpunkt der Aufnahme");
		lbl_FirstVisit.setBounds(215, 447, 305, 14);
		contentPanel.add(lbl_FirstVisit);
		
		JLabel lbl_labor_body = new JLabel("Laborwerte zum Zeitpunkt der k\u00F6rperlichen Untersuchung");
		lbl_labor_body.setBounds(215, 504, 295, 14);
		contentPanel.add(lbl_labor_body);
		
		JLabel lblChdparameterZumZeitpunkt = new JLabel("CHD-Parameter zum Zeitpunkt der k\u00F6rperlichen Untersuchung");
		lblChdparameterZumZeitpunkt.setBounds(211, 563, 317, 14);
		contentPanel.add(lblChdparameterZumZeitpunkt);
		
		JLabel lblFollowUp = new JLabel("Follow up");
		lblFollowUp.setBounds(228, 635, 46, 14);
		contentPanel.add(lblFollowUp);
		
		txtf_followUP = new JTextField();
		txtf_followUP.setBounds(659, 632, 86, 20);
		contentPanel.add(txtf_followUP);
		txtf_followUP.setColumns(10);
		
		txtf_CHD = new JTextField();
		txtf_CHD.setBounds(659, 560, 86, 20);
		contentPanel.add(txtf_CHD);
		txtf_CHD.setColumns(10);
		
		txtf_laborBody = new JTextField();
		txtf_laborBody.setBounds(659, 501, 86, 20);
		contentPanel.add(txtf_laborBody);
		txtf_laborBody.setColumns(10);
		
		txtf_LaborFirstVisit = new JTextField();
		txtf_LaborFirstVisit.setBounds(659, 444, 86, 20);
		contentPanel.add(txtf_LaborFirstVisit);
		txtf_LaborFirstVisit.setColumns(10);
		
		txtf_Expodata = new JTextField();
		txtf_Expodata.setBounds(659, 392, 86, 20);
		contentPanel.add(txtf_Expodata);
		txtf_Expodata.setColumns(10);
		
		txtf_Stammdata = new JTextField();
		txtf_Stammdata.setBounds(659, 339, 86, 20);
		contentPanel.add(txtf_Stammdata);
		txtf_Stammdata.setColumns(10);
		
		JLabel lblAnzahlDerAusgefllten = new JLabel("Anzahl der ausgef\u00FCllten Formulare:");
		lblAnzahlDerAusgefllten.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAnzahlDerAusgefllten.setBounds(331, 298, 372, 14);
		contentPanel.add(lblAnzahlDerAusgefllten);
		
		txtDate = new JTextField();
		txtDate.setEditable(false);
		txtDate.setBounds(293, 253, 145, 20);
		contentPanel.add(txtDate);
		txtDate.setColumns(10);
	}



}
