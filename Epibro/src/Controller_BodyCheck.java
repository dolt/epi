import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class Controller_BodyCheck {

	private Model model;
	private View_BodyCheck viewBodyCheck;
	private View_FollowUP viewFollowUP;

	public Controller_BodyCheck(View_FollowUP viewFollowUP,
			View_BodyCheck viewBodyCheck, Model model) {
		this.model = model;
		this.viewBodyCheck = viewBodyCheck;
		this.viewFollowUP = viewFollowUP;

		// adding listeners addBackButtonListener
		// viewEditMember.addMinusButtonListener(new MinusButtonListener());
		viewBodyCheck.addFwardButtonListener(new FwardButtonListner());
		viewBodyCheck.addBackButtonListener(new BackButtonListener());

	}

	public class BackButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent a) {
			
			
			
			// Plausibilitätschecks....
						// ...auf "leere Felder"....
						if (model.isNull(viewBodyCheck.getTextField_dia())
								|| model.isNull(viewBodyCheck.getDateChooser())
								|| model.isNull(viewBodyCheck.getTextField_puls())
								|| model.isNull(viewBodyCheck.getTextFieldsystol())
								|| viewBodyCheck.getRdbtnDieag_N() == false
								&& viewBodyCheck.getRdbtnDieag_Y() == false
								|| model.isNull(viewBodyCheck.getDateChooser())) {
							JOptionPane.showMessageDialog(null,
									"Bitte füllen Sie alle Felder aus!");
						}
						// Is das nummer?

						else if (model.isNumber(viewBodyCheck.getTextField_dia()) == false
								|| model.isNumber(viewBodyCheck.getTextFieldsystol()) == false
								|| model.isNumber(viewBodyCheck.getTextField_puls()) == false) {
							JOptionPane.showMessageDialog(null,
									"Es befinden sich unerlaubte Zeichen in den Feldern!");
						}
						// rANge?

						else if (model.isOutOfRange(viewBodyCheck.getTextField_dia(), 30,
								200)
								|| model.isOutOfRange(viewBodyCheck.getTextFieldsystol(),
										30, 200)) {

							JOptionPane.showMessageDialog(null,
									"Blutdruck ausserhalb des möglichen Bereiches!");
						}
						// medizinshit irgendwas mit Blut

						else if (model.secondSmallerThanFirst(
								viewBodyCheck.getTextFieldsystol(),
								viewBodyCheck.getTextField_dia())) {
							JOptionPane.showMessageDialog(null,
									"Systolischer Blutdruck höher als Diastolischer");

						}
						// lebt der typ noch?

						else if (model.isOutOfRange(viewBodyCheck.getTextField_puls(), 40,
								200)) {

							JOptionPane
									.showMessageDialog(
											null,
											"!ACHTUNG!"
													+ "\n"
													+ "Puls liegt ausserhalb des physiologischen Messbereiches!");
						}

						else {
							// speicher die eingegebenen Daten in die Klasse Stammdaten....
							OwnClass_BodyCheck neuerBodyCheckabernichtdervomWerseln = new OwnClass_BodyCheck(
									viewBodyCheck.getTf_ID(),
									viewBodyCheck.getDateChooser(),
									model.booleantoSTring(viewBodyCheck.getRdbtnDieag_Y()), // eZ
									viewBodyCheck.getTextFieldsystol(),
									viewBodyCheck.getTextField_dia(),
									viewBodyCheck.getTextField_puls());

						}

			// eigene Klasse

			viewBodyCheck.setVisible(false);
			viewFollowUP.setVisible(true);
			viewFollowUP.setLocation(null);

		}

	}

	public class FwardButtonListner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) { // sys > dia

			// Plausibilitätschecks....
			// ...auf "leere Felder"....
			if (model.isNull(viewBodyCheck.getTextField_dia())
					|| model.isNull(viewBodyCheck.getDateChooser())
					|| model.isNull(viewBodyCheck.getTextField_puls())
					|| model.isNull(viewBodyCheck.getTextFieldsystol())
					|| viewBodyCheck.getRdbtnDieag_N() == false
					&& viewBodyCheck.getRdbtnDieag_Y() == false
					|| model.isNull(viewBodyCheck.getDateChooser())) {
				JOptionPane.showMessageDialog(null,
						"Bitte füllen Sie alle Felder aus!");
			}
			// Is das nummer?

			else if (model.isNumber(viewBodyCheck.getTextField_dia()) == false
					|| model.isNumber(viewBodyCheck.getTextFieldsystol()) == false
					|| model.isNumber(viewBodyCheck.getTextField_puls()) == false) {
				JOptionPane.showMessageDialog(null,
						"Es befinden sich unerlaubte Zeichen in den Feldern!");
			}
			// rANge?

			else if (model.isOutOfRange(viewBodyCheck.getTextField_dia(), 30,
					200)
					|| model.isOutOfRange(viewBodyCheck.getTextFieldsystol(),
							30, 200)) {

				JOptionPane.showMessageDialog(null,
						"Blutdruck ausserhalb des möglichen Bereiches!");
			}
			// medizinshit irgendwas mit Blut

			else if (model.secondSmallerThanFirst(
					viewBodyCheck.getTextFieldsystol(),
					viewBodyCheck.getTextField_dia())) {
				JOptionPane.showMessageDialog(null,
						"Systolischer Blutdruck höher als Diastolischer");

			}
			// lebt der typ noch?

			else if (model.isOutOfRange(viewBodyCheck.getTextField_puls(), 40,
					200)) {

				JOptionPane
						.showMessageDialog(
								null,
								"!ACHTUNG!"
										+ "\n"
										+ "Puls liegt ausserhalb des physiologischen Messbereiches!");
			}

			else {
				// speicher die eingegebenen Daten in die Klasse Stammdaten....
				OwnClass_BodyCheck neuerBodyCheckabernichtdervomWerseln = new OwnClass_BodyCheck(
						viewBodyCheck.getTf_ID(),
						viewBodyCheck.getDateChooser(),
						model.booleantoSTring(viewBodyCheck.getRdbtnDieag_Y()), // eZ
						viewBodyCheck.getTextFieldsystol(),
						viewBodyCheck.getTextField_dia(),
						viewBodyCheck.getTextField_puls());

			}
			viewBodyCheck.setVisible(false);
			viewFollowUP.setVisible(true);
			viewFollowUP.setLocation(null);
			// eigen klasse

		}

	}

}
