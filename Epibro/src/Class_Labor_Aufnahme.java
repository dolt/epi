
public class Class_Labor_Aufnahme {
	
	private String pat_id;
	private String datum_labor_aufnahme;
	private String cholesterin_aufnahme;
	private String creatinin_aufnahme;
	private String triglyceride_aufnahme;
	private String bsg1_aufnahme;
	private String bsg2_aufnahme;
	
	
	
	public Class_Labor_Aufnahme(String pat_id, String datum_labor_aufnahme, String cholesterin_aufnahme, String creatinin_aufnahme, String triglyceride_aufnahme, String bsg1_aufnahme, String bsg2_aufnahme)
	{
		
		this.pat_id					= pat_id;
		this.datum_labor_aufnahme	= datum_labor_aufnahme;
		this.cholesterin_aufnahme	= cholesterin_aufnahme;
		this.creatinin_aufnahme		= creatinin_aufnahme;
		this.triglyceride_aufnahme	= triglyceride_aufnahme;
		this.bsg1_aufnahme			= bsg1_aufnahme;
		this.bsg2_aufnahme			= bsg2_aufnahme;
		
		
		
	}



	public String getPat_id() {
		return pat_id;
	}



	public void setPat_id(String pat_id) {
		this.pat_id = pat_id;
	}



	public String getDatum_labor_aufnahme() {
		return datum_labor_aufnahme;
	}



	public void setDatum_labor_aufnahme(String datum_labor_aufnahme) {
		this.datum_labor_aufnahme = datum_labor_aufnahme;
	}



	public String getCholesterin_aufnahme() {
		return cholesterin_aufnahme;
	}



	public void setCholesterin_aufnahme(String cholesterin_aufnahme) {
		this.cholesterin_aufnahme = cholesterin_aufnahme;
	}



	public String getCreatinin_aufnahme() {
		return creatinin_aufnahme;
	}



	public void setCreatinin_aufnahme(String creatinin_aufnahme) {
		this.creatinin_aufnahme = creatinin_aufnahme;
	}



	public String getTriglyceride_aufnahme() {
		return triglyceride_aufnahme;
	}



	public void setTriglyceride_aufnahme(String triglyceride_aufnahme) {
		this.triglyceride_aufnahme = triglyceride_aufnahme;
	}



	public String getBsg1_aufnahme() {
		return bsg1_aufnahme;
	}



	public void setBsg1_aufnahme(String bsg1_aufnahme) {
		this.bsg1_aufnahme = bsg1_aufnahme;
	}



	public String getBsg2_aufnahme() {
		return bsg2_aufnahme;
	}



	public void setBsg2_aufnahme(String bsg2_aufnahme) {
		this.bsg2_aufnahme = bsg2_aufnahme;
	}
	
	
	
	

}
