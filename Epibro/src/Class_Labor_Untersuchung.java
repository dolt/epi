

public class Class_Labor_Untersuchung {
	
	
	private String pat_id;
	private String datum_labor_untersuchung;
	private String cholesterin_untersuchung;
	private String creatinin_untersuchung;
	private String triglyceride_Untersuchung;
	private String bsg1_untersuchung;
	private String bsg2_untersuchung;
	
	
	public Class_Labor_Untersuchung(String pat_id, String datum_labor_untersuchung, String cholesterin_untersuchung,
			String creatinin_untersuchung, String triglyceride_Untersuchung, String bsg1_untersuchung, String bsg2_untersuchung)
	{
		
		this.pat_id						= pat_id;
		this.datum_labor_untersuchung 	= datum_labor_untersuchung;
		this.cholesterin_untersuchung 	= cholesterin_untersuchung;
		this.creatinin_untersuchung		= creatinin_untersuchung;
		this.triglyceride_Untersuchung 	= triglyceride_Untersuchung;
		this.bsg1_untersuchung			= bsg1_untersuchung;
		this.bsg2_untersuchung			= bsg2_untersuchung;
		
		
		
	}

	
	
	

	public String getPat_id() {
		return pat_id;
	}





	public void setPat_id(String pat_id) {
		this.pat_id = pat_id;
	}





	public String getDatum_labor_untersuchung() {
		return datum_labor_untersuchung;
	}


	public void setDatum_labor_untersuchung(String datum_labor_untersuchung) {
		this.datum_labor_untersuchung = datum_labor_untersuchung;
	}


	public String getCholesterin_untersuchung() {
		return cholesterin_untersuchung;
	}


	public void setCholesterin_untersuchung(String cholesterin_untersuchung) {
		this.cholesterin_untersuchung = cholesterin_untersuchung;
	}


	public String getCreatinin_untersuchung() {
		return creatinin_untersuchung;
	}


	public void setCreatinin_untersuchung(String creatinin_untersuchung) {
		this.creatinin_untersuchung = creatinin_untersuchung;
	}


	public String getTriglyceride_Untersuchung() {
		return triglyceride_Untersuchung;
	}


	public void setTriglyceride_Untersuchung(String triglyceride_Untersuchung) {
		this.triglyceride_Untersuchung = triglyceride_Untersuchung;
	}


	public String getBsg1_untersuchung() {
		return bsg1_untersuchung;
	}


	public void setBsg1_untersuchung(String bsg1_untersuchung) {
		this.bsg1_untersuchung = bsg1_untersuchung;
	}


	public String getBsg2_untersuchung() {
		return bsg2_untersuchung;
	}


	public void setBsg2_untersuchung(String bsg2_untersuchung) {
		this.bsg2_untersuchung = bsg2_untersuchung;
	}
	
	
	

}
