import java.awt.List;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

public class Model {

	private static final String INITIAL_VALUE = "";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	final static String EMPTY = "";

	// gibt leeren String zur�ck

	public String clearTextField() {
		return EMPTY;

	}

	// pr�ft ob Feld leer ist

	public boolean isNull(String entry) {

		if (entry.equals("")) {
			return true;
		} else {
			return false;
		}

	}

	// pr�ft ob ein zweiter Wert kleiner ist als der erste (zB bei BSG NICHT
	// m�glich...)

	public boolean secondSmallerThanFirst(String entry1, String entry2) {

		int value1 = Integer.parseInt(entry1);
		int value2 = Integer.parseInt(entry2);

		if (value1 > value2) {
			return true;
		}

		else {
			return false;
		}

	}

	// pr�ft eine benutzerdefinierbare Range

	public boolean isOutOfRange(String entry, int min, int max) {

		int value = Integer.parseInt(entry);

		if (value < min || value > max) {

			return true;

		} else {
			return false;
		}

	}

	// pr�ft ob es sich bei dem Inhalt um eine Zahl handelt!

	public boolean isNumber(String entry) {

		if (entry.matches("\\d*") || entry.matches(".")) {

			return true;

		} else {

			return false;
		}

	}

	public String getDate() {

		GregorianCalendar now = new GregorianCalendar();
		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
		String date = df.format(now.getTime());
		// df=DateFormat.getTimeInstance(DateFormat.SHORT);
		// String time = df.format(now.getTime());

		String dateTime = "Date: " + date;

		return dateTime;
	}
	public String booleantoSTring(boolean bool){
		if(bool)
		return "ja";
		return "nein";
		
	}

}
