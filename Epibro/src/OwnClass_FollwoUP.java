
public class OwnClass_FollwoUP {

	private String Date;
	private boolean infarktYN;
	private boolean diedyetYN;
	private String InfarktDate;
	
	
	public OwnClass_FollwoUP(String Date, boolean infarkt, boolean diedyet, String infarktDate ){
		setDate(Date);
		setInfarktYN(infarkt);
		setDiedyetYN(diedyet);
		setInfarktDate(infarktDate);
		
		
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return Date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		Date = date;
	}

	/**
	 * @return the infarktYN
	 */
	public boolean isInfarktYN() {
		return infarktYN;
	}

	/**
	 * @param infarktYN the infarktYN to set
	 */
	public void setInfarktYN(boolean infarktYN) {
		this.infarktYN = infarktYN;
	}

	
	/**
	 * @return the infarktDate
	 */
	public String getInfarktDate() {
		return InfarktDate;
	}

	/**
	 * @param infarktDate the infarktDate to set
	 */
	public void setInfarktDate(String infarktDate) {
		InfarktDate = infarktDate;
	}

	/**
	 * @return the diedyetYN
	 */
	public boolean isDiedyetYN() {
		return diedyetYN;
	}

	/**
	 * @param diedyetYN the diedyetYN to set
	 */
	public void setDiedyetYN(boolean diedyetYN) {
		this.diedyetYN = diedyetYN;
	}

}
