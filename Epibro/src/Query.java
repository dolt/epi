


import java.awt.List;
import java.sql.*;

import javax.swing.JOptionPane;




public class Query {
	
	
	
	public static Connection con = null;
	private static String id_user = null;
	private static String pat_id = null;
	
	
	
	
	public static void storeBodycheck(String pat_id, String datum, String diagnose, String syst, String diast, String puls, String user_id)
	{
		
		

		con = DBConnection.connectToDb();
		
		String query = "CALL epi_chd_save(" + pat_id + ",'"+ datum + "','" + diagnose +"','"+ syst + "','"+ diast+"','"+puls +"','"+user_id+ ")";
		 
		
		
		
		try{
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			JOptionPane.showMessageDialog(null, "Stammdaten gespeichert");
			
		}catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Stammdaten  failed");
		}
			
			
		finally{
			try
			{
				con.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
	}
	
	
	
	
	
	public static Class_Expo_Aufnahme loadExpoAufnahme(String fall_id, Class_Expo_Aufnahme expo)
	{
		
		con = DBConnection.connectToDb();
		
		String query = "CALL epi_exposition_load(" + fall_id + ")";
		
		
		try{
			
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			while(rs.next())
			{
				//zu �bergebende Klasse wird mit rs.getString bef�llt
				
				//expo.setDatum_stammdat(rs.getString(1));
				expo.setDatum_expo_aufnahme(rs.getString(1));
				expo.setRauchen(rs.getString(2));
				expo.setRauchenArt(rs.getString(3));
				expo.setRauchenBeginn(rs.getString(4));
				expo.setRauchenEnde(rs.getString(5));
				expo.setRauchenAnzahl(rs.getString(6));
				expo.setAlkoholH�ufigkeit(rs.getString(7));
				expo.setAlkoholGramm(rs.getString(8));
				expo.setGesundheitlicheBedenken(rs.getString(9));
			}
		}
		catch(Exception e)
		{
			
			JOptionPane.showMessageDialog(null, e);
			
			
		}
		
		//bef�llte Klasse zur�ckgeben
		return expo;
		
		
	}
	
	
	
	
	//vom Typ Klasse												bekommt eine 'geleerte' Klasse �bergeben
	public static Class_Stammdaten loadStammdaten(String fall_id, Class_Stammdaten stammdat)
	{
		
		con = DBConnection.connectToDb();
		
		String query = "CALL epi_stammdaten_load(" + fall_id + ")";
		
		
		try{
			
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			while(rs.next())
			{
				//zu �bergebende Klasse wird mit rs.getString bef�llt
				
				stammdat.setDatum_stammdat(rs.getString(1));
				stammdat.setZentrum_stammdat(rs.getString(2));
				stammdat.setGeburt_stammdat(rs.getString(3));
				stammdat.setGroesse_cm_stammdat(rs.getString(4));
				stammdat.setGewicht_kg_stammdat(rs.getString(5));
				
			}
			
			
		}
		catch(Exception e)
		{
			
			JOptionPane.showMessageDialog(null, "Nix da");
			
			
		}
		
		//bef�llte Klasse zur�ckgeben
		return stammdat;
		
		
	}
	
	
	
	
	
	
	
	
	public static List alleFaelleComboBox()
	{
		
		con = DBConnection.connectToDb();
		
		List list = new List();
		
		
		
		String query = "CALL epi_get_faelle();";
		
		try
		{
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			while(rs.next())
			{
				
				
				list.add(rs.getString(1));
				
			}
			
			
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "nix da");
		}
		
		
		return list;
		
	}
	
	
	
	
	
	
	
	
	
	public static String isLoginValid(String username, String password) 
	{

		con = DBConnection.connectToDb(); //get Connection to DB
		
		
		String query = "SELECT * FROM user WHERE e_mail = '" +username + "' AND password = '" + password +"'";
		
		
		int count = 0;
		
		
		try
		{
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
	
			while(rs.next())
				{
					count++; //count up every time a new ResultSet is committed
					id_user = rs.getString("id_user");
					
					
				}
			
			if(count == 1) // ==1 , then exactly one entry in database
				{
					return id_user;
				}
			else //if bigger or smaller then one, no permission to login
				{
					return null;
				}
			
		}
		
		catch(Exception e)
		{
			
			return null;
				
		}
		
		finally
		
		{
			try {
				
				con.close(); //either the method works or not... the connection will always be closed
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	
	
	public static String getNewID()
	{
		
		con			 = DBConnection.connectToDb();
		String query = "CALL epi_id_patient()";
		
		
		
		try{
			
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			
			while(rs.next())
			{
				pat_id = rs.getString(1);
				
			}
			
		}
		catch(Exception e)
		{
			
			JOptionPane.showMessageDialog(null, "nix da");
			
			
		}
		return pat_id;
		
		
	}

	
	
	
	
	public static void storeStammdaten(String id, String datum, String zentrum, String geb, String groesse, String gewicht, String user  )
	{
		
		con = DBConnection.connectToDb();
		
		String query = "CALL epi_stammdaten_save" + "(" + id + ",'" + datum + "','" + zentrum + "','" + geb + "','" + groesse + "','" + gewicht + "'," + user + ");";
		
		
		
		
		try{
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			JOptionPane.showMessageDialog(null, "Stammdaten gespeichert");
			
		}catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Stammdaten  failed");
		}
			
			
		finally{
			try
			{
				con.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		
		
	}
	
	
	
	public static void storeExposition(String pat_id ,String datum_expo_aufnahme, String rauchen, String rauchenArt, String rauchenBeginn, String rauchenEnde, String rauchenAnzahl, String  alkoholH�ufigkeit, String alkoholGramm, String gesundheitlicheBedenken, String user_id)
	{
		
		con = DBConnection.connectToDb();
		
		String query = "CALL epi_exposition_save(" + pat_id + ", '" + datum_expo_aufnahme + "', '" + rauchen + "', '" + rauchenArt + "', '" + rauchenBeginn + "', '" + rauchenEnde + "', '" + rauchenAnzahl + "', '" + alkoholH�ufigkeit + "', '" + alkoholGramm + "', '" + gesundheitlicheBedenken + "', " + user_id +  ");";   
		
		
		
		
		try{
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			
			
			JOptionPane.showMessageDialog(null, "Exposition Gespeichert!");
			
		}catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Exposition failed hard" + e);
		}
			
			
		finally{
			try
			{
				con.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		
		
	}
	
	
//String art kann untersuchung , aufnahme
	
	public static void storeLabor(String art, String pat_id, String datum, String cholesterin, String creatinin, String triglyceride, String bsg1, String bsg2, String user_id)
	{
		
		con = DBConnection.connectToDb();
		String query = "CALL epi_labor_" + art + "_save(" + pat_id + ",'" + datum + "', '" + cholesterin + "', '" + creatinin + "', '" + triglyceride + "', '" + bsg1 + "', '" + bsg2 + "', " + user_id + ");" ;
		
		
		
		try{
			
			
			
			PreparedStatement pst	= con.prepareStatement(query);
			ResultSet rs			= pst.executeQuery();
			
			
			JOptionPane.showMessageDialog(null, "Labor erfolgreich");
			
		}catch(Exception e){
		
			JOptionPane.showMessageDialog(null, "Labor failed hard" + e);
		

			
			
		}finally{
			
			try
			{
				con.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
		}
			
			
		
	}
	
	
	

}
