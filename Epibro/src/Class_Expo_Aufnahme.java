

public class Class_Expo_Aufnahme {

	private String pat_id;
	private String datum_expo_aufnahme;
	private String rauchen;
	private String rauchenArt;
	private String rauchenBeginn;
	private String rauchenEnde;
	private String rauchenAnzahl;
	private String  alkoholHäufigkeit;
	private String alkoholGramm;
	private String gesundheitlicheBedenken;
	
	
	
	public Class_Expo_Aufnahme(String pat_id ,String datum_expo_aufnahme, String rauchen, String rauchenArt, String rauchenBeginn, String rauchenEnde, String rauchenAnzahl, String  alkoholHäufigkeit, String alkoholGramm, String gesundheitlicheBedenken)
	{
		

		this.pat_id					= pat_id;
		this.datum_expo_aufnahme 	= datum_expo_aufnahme;
		this.rauchen				= rauchen;
		this.rauchenArt				= rauchenArt;
		this.rauchenBeginn			= rauchenBeginn;
		this.rauchenEnde			= rauchenEnde;	
		this.rauchenAnzahl			= rauchenAnzahl;
		this.alkoholHäufigkeit		= alkoholHäufigkeit;
		this.alkoholGramm			= alkoholGramm;
		this.gesundheitlicheBedenken = gesundheitlicheBedenken;
		
		
		
	}



	public String getPat_id() {
		return pat_id;
	}



	public void setPat_id(String pat_id) {
		this.pat_id = pat_id;
	}



	public String getDatum_expo_aufnahme() {
		return datum_expo_aufnahme;
	}



	public void setDatum_expo_aufnahme(String datum_expo_aufnahme) {
		this.datum_expo_aufnahme = datum_expo_aufnahme;
	}



	public String getRauchen() {
		return rauchen;
	}



	public void setRauchen(String rauchen) {
		this.rauchen = rauchen;
	}



	public String getRauchenArt() {
		return rauchenArt;
	}



	public void setRauchenArt(String rauchenArt) {
		this.rauchenArt = rauchenArt;
	}



	public String getRauchenBeginn() {
		return rauchenBeginn;
	}



	public void setRauchenBeginn(String rauchenBeginn) {
		this.rauchenBeginn = rauchenBeginn;
	}



	public String getRauchenEnde() {
		return rauchenEnde;
	}



	public void setRauchenEnde(String rauchenEnde) {
		this.rauchenEnde = rauchenEnde;
	}



	public String getRauchenAnzahl() {
		return rauchenAnzahl;
	}



	public void setRauchenAnzahl(String rauchenAnzahl) {
		this.rauchenAnzahl = rauchenAnzahl;
	}



	public String getAlkoholHäufigkeit() {
		return alkoholHäufigkeit;
	}



	public void setAlkoholHäufigkeit(String alkoholHäufigkeit) {
		this.alkoholHäufigkeit = alkoholHäufigkeit;
	}



	public String getAlkoholGramm() {
		return alkoholGramm;
	}



	public void setAlkoholGramm(String alkoholGramm) {
		this.alkoholGramm = alkoholGramm;
	}



	public String getGesundheitlicheBedenken() {
		return gesundheitlicheBedenken;
	}



	public void setGesundheitlicheBedenken(String gesundheitlicheBedenken) {
		this.gesundheitlicheBedenken = gesundheitlicheBedenken;
	}
	
	
	
	
	
	
}
