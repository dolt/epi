import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class Controller_FollowUp {

	private Model model;
	private View_FollowUP viewFollowUP;
	private View_BodyCheck view_Bodycheck;
	

	// StorageFactory storagerfac = new StorageFactory();
	// StorageInterface interfaceDB = storagerfac.getStorage();

	public Controller_FollowUp(View_FollowUP viewFollowUP,View_BodyCheck viewBodycheck, Model model) {
		this.model = model;
		this.viewFollowUP = viewFollowUP;
		this.view_Bodycheck = viewBodycheck;
		
		// viewEditMember.addMinusButtonListener(new MinusButtonListener());
		viewFollowUP.addFwardButtonListener(new FwardButtonListner());
		viewFollowUP.addBackButtonListener(new BackButtonListener());
		viewFollowUP.addDiedYet_NButtonListener(new DiedYetNButtonListener());
		viewFollowUP.addDiedYet_YButtonListener(new DiedYetYButtonListener());
		viewFollowUP.addrdbtn_Infarkt_NButtonListener(new InfarktNButtonListener());
		viewFollowUP.addrdbtn_Infarkt_YButtonListener(new InfarktYButtonListener());

	}
	

	public class InfarktNButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			viewFollowUP.hideInfaktDate();
		
		}

	}

	public class InfarktYButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			 viewFollowUP.showInfaktDate();

		}

	}

	public class DiedYetNButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			// TODO Auto-generated method stub

		}

	}

	public class DiedYetYButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

		}

	}

	public class FwardButtonListner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
				if((viewFollowUP.getRdbtn_Died_N()||viewFollowUP.getRdbtn_Died_Y())&&(viewFollowUP.getRdbtn_Infarkt_N()||viewFollowUP.getRdbtn_Infarkt_Y())&&!model.isNull(viewFollowUP.getDatumChooser().toString())){	
					OwnClass_FollwoUP newFollowUP = new OwnClass_FollwoUP(null, false, false, ".");
					newFollowUP.setDate(viewFollowUP.getDatumChooser().toString());
					newFollowUP.setDiedyetYN(viewFollowUP.getRdbtn_Died_Y());
					newFollowUP.setInfarktYN(viewFollowUP.getRdbtn_Infarkt_Y());
					newFollowUP.setInfarktDate(viewFollowUP.getInfarktDateChooser().toString());
					
					
				
				}
				else
					JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Felder aus!");

		}
	}

	public class BackButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			OwnClass_FollwoUP newFollowUP = new OwnClass_FollwoUP(null, false, false, ".");
			newFollowUP.setDate(viewFollowUP.getDatumChooser().toString());
			newFollowUP.setDiedyetYN(viewFollowUP.getRdbtn_Died_Y());
			newFollowUP.setInfarktYN(viewFollowUP.getRdbtn_Infarkt_Y());
			newFollowUP.setInfarktDate(viewFollowUP.getInfarktDateChooser().toString());
			view_Bodycheck.setVisible(true);
			viewFollowUP.setVisible(false);
		

		}
	}

}
