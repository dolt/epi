

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class DBConnection 
{
	
	
	
	static Connection con = null;
	static String classpath = "com.mysql.jdbc.Driver"; //depends on which database you are using -> in this case mySQL
	static String urlDatabase = "jdbc:mysql://i-intra-03.informatik.hs-ulm.de/md_proj_04";
	static String username = "MD_PROJ_04";
	static String password = "MD_PROJ_04";
	
// 	jdbc:mysql://host/database
	
	/**
	 *connects to a Database
	 *
	 *@return Connection or null, depending if a connection is possible
	 **/
		public static Connection connectToDb()
		{
			
			try
			{
				Class.forName(classpath); //load the classpath
				con = DriverManager.getConnection(urlDatabase, username, password); //use the DriverManager to connect with the database by using password and username
				
				return con; //returns the Connection
			}
			catch(Exception e) //..if something goes wrong...
			{
				JOptionPane.showMessageDialog(null, "No Connection to Database! WLAN/VPN?");
				
				return null;
			}
			
		}
	
	

}

