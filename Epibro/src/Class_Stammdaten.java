
public class Class_Stammdaten {
	
	
	
	private String pat_id;
	private String datum_stammdat;
	private String zentrum_stammdat;
	private String geburt_stammdat;
	private String groesse_cm_stammdat;
	private String gewicht_kg_stammdat;
	
	
	public Class_Stammdaten(String pat_id ,String datum_stammdat, String zentrum_stammdat, String geburt_stammdat, String groesse_cm_stammdat, 
			String gewicht_kg_stammdat)
	{
		
		
		
		this.pat_id					= pat_id;
		this.datum_stammdat			= datum_stammdat;
		this.zentrum_stammdat		= zentrum_stammdat;
		this.geburt_stammdat		= geburt_stammdat;
		this.groesse_cm_stammdat	= groesse_cm_stammdat;
		this.gewicht_kg_stammdat	= gewicht_kg_stammdat;
		
	}


	public String getPat_id() {
		return pat_id;
	}


	public void setPat_id(String pat_id) {
		this.pat_id = pat_id;
	}


	public String getDatum_stammdat() {
		return datum_stammdat;
	}


	public void setDatum_stammdat(String datum_stammdat) {
		this.datum_stammdat = datum_stammdat;
	}


	public String getZentrum_stammdat() {
		return zentrum_stammdat;
	}


	public void setZentrum_stammdat(String zentrum_stammdat) {
		this.zentrum_stammdat = zentrum_stammdat;
	}


	public String getGeburt_stammdat() {
		return geburt_stammdat;
	}


	public void setGeburt_stammdat(String geburt_stammdat) {
		this.geburt_stammdat = geburt_stammdat;
	}


	public String getGroesse_cm_stammdat() {
		return groesse_cm_stammdat;
	}


	public void setGroesse_cm_stammdat(String groesse_cm_stammdat) {
		this.groesse_cm_stammdat = groesse_cm_stammdat;
	}


	public String getGewicht_kg_stammdat() {
		return gewicht_kg_stammdat;
	}


	public void setGewicht_kg_stammdat(String gewicht_kg_stammdat) {
		this.gewicht_kg_stammdat = gewicht_kg_stammdat;
	}


	
	

}
